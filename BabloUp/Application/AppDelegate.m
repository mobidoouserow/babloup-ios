//
//  AppDelegate.m
//  BabloUp
//
//  Created by Pavel Wasilenko on 05/04/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import "AppDelegate.h"

#if HAS_POD(CocoaLumberjack)
#import <CocoaLumberjack/CocoaLumberjack.h>

#if HAS_POD(Sidecar)
#import <Sidecar/CRLMethodLogFormatter.h>
#endif

#if HAS_POD(Fabric)
#import <Fabric/Fabric.h>
#endif

#if HAS_POD(Crashlytics)
#import <Crashlytics/Crashlytics.h>
#endif

//#if HAS_POD(CrashlyticsLumberjack)
#import <CrashlyticsLumberjack/CrashlyticsLogger.h>
//#endif

#endif

#import <Firebase/Firebase.h>

//#import <Batch/Batch.h>

//#import <AppLovinSDK/AppLovinSDK.h>

//#import <FyberSDK/FyberSDK.h>

//#import "NativeXSDK.h"

@interface AppDelegate ()


@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [self initializeLogging];
    
    [self initializeDataManager];
    
    [self showInitialController];
    
//    [self initBatch];
    
//    [self initAppLovin];
    
//    [self initFyber];
    
//    [self initNavitex];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    
}


#pragma mark - Initialization

/**
 Sets up RouterController
 */
- (void)showInitialController
{
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    
    self.window.rootViewController = [[BUDataManager instance].router sideMenuController];
    
    [self.window makeKeyAndVisible];
    
    [[BUDataManager instance].router showInitialViewController];
}

- (void)initializeDataManager
{
    self.dataManager = [BUDataManager instance];
}

/**
 Sets up CocoaLumberjack
 */
-(void)initializeLogging
{
#if HAS_POD(CocoaLumberjack)
#if HAS_POD(Sidecar)
    CRLMethodLogFormatter *logFormatter = [[CRLMethodLogFormatter alloc] init];
    [[DDASLLogger sharedInstance] setLogFormatter:logFormatter];
    [[DDTTYLogger sharedInstance] setLogFormatter:logFormatter];
    
//    #if HAS_POD(CrashlyticsLumberjack)
        [[CrashlyticsLogger sharedInstance] setLogFormatter:logFormatter];
//    #endif
#endif
    
    // Emulate NSLog behavior for DDLog*
    [DDLog addLogger:[DDASLLogger sharedInstance]];
    [DDLog addLogger:[DDTTYLogger sharedInstance]];
//    #if HAS_POD(CrashlyticsLumberjack)
        [DDLog addLogger:[CrashlyticsLogger sharedInstance]];
//    #endif
#endif
    
    [Fabric with:@[[Crashlytics class]]];

    [FIRApp configure];
}

//- (void)initBatch;
//{
//    // Start Batch.
//    // TODO : switch to live api key before store release
////    [Batch startWithAPIKey:@"DEV5988071A3DD835C42CCC0886E28"]; // dev
//     [Batch startWithAPIKey:@"5988071A3D71C63DDBC25898EF0D0C"]; // live
//    
//    // Register for push notifications
//    [BatchPush registerForRemoteNotifications];
//}

//- (void)initAppLovin;
//{
//    [ALSdk initializeSdk];
//    [ALSdk shared].userIdentifier = BUDATAMANAGER.currentUser.access_token;
//}

//- (void)initFyber;
//{
//    FYBSDKOptions *options = [FYBSDKOptions optionsWithAppId:@"106367"
//                                               securityToken:@"5ff0b285586397645f8d4f897636c202"];
//    [FyberSDK startWithOptions:options];
//}

//- (void)initNavitex;
//{
//    [NativeXSDK initializeWithAppId:@"140098"];
//}

#pragma mark - Social

//iOS 9 workflow
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options {
//    [VKSdk processOpenURL:url fromApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]];
    return YES;
}

//iOS 8 and lower
-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
//    [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
//    [VKSdk processOpenURL:url fromApplication:sourceApplication];
    return YES;
}

@end
