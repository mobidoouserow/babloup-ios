//
//  AppDelegate.h
//  BabloUp
//
//  Created by Pavel Wasilenko on 05/04/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BUDataManager.h"
#import "BURouterController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) BUDataManager *dataManager;

@property (strong, nonatomic) BURouterController *navigationController;

@end

