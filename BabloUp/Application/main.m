//
//  main.m
//  BabloUp
//
//  Created by Pavel Wasilenko on 05/04/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
