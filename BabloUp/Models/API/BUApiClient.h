//
//  BUApiClient.h
//  BabloUp
//
//  Created by Pavel Wasilenko on 10/04/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//



#import <AFNetworking.h>

typedef void (^ApiSuccessBlock)(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject);
typedef void (^ApiFailureBlock)(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull  error);

@interface BUApiClient : AFHTTPSessionManager

//NS_ASSUME_NONNULL_BEGIN

- (void)postAuth:(NSString * _Nullable)token
      forService:(NSString * _Nullable)service
         success:(ApiSuccessBlock _Nullable)success
         failure:(ApiFailureBlock _Nullable)failure;

- (void)getProfileSuccess:(ApiSuccessBlock _Nullable)success
                  failure:(ApiFailureBlock _Nullable)failure;

- (void)postReferrer:(NSString * _Nullable)referrer_code
             success:(ApiSuccessBlock _Nullable)success
             failure:(ApiFailureBlock _Nullable)failure;


#pragma mark - Offers related

/**
 Офферы / Получить список офферов (с количеством на страницу по умолчанию)
 
 GET offers?page=1&per_page=100&status=new&os=ios&access_token=
 
 @param page - NSNumber - Номер страницы. Example: 2. Integer
 
 @param status - Фильтр по статусу офферов. Possible values:  new , active , completed.
 */
- (void)getOffersPage:(NSNumber * _Nullable)page
               status:(ApiClientOfferStatus)status
              success:(ApiSuccessBlock _Nullable)success
              failure:(ApiFailureBlock _Nullable)failure;

/**
 Офферы / Получить список офферов
 
 GET offers?page=1&per_page=100&status=new&os=ios&access_token=
 
 @param page - номер страницы
 @param per_page - офферов на страницу
 @param status - status description
 @param success - success блок
 @param failure - failure блок
 */
- (void)getOffersPage:(NSNumber * _Nullable)page
             per_page:(NSNumber * _Nullable)per_page
               status:(ApiClientOfferStatus)status
              success:(ApiSuccessBlock _Nullable)success
              failure:(ApiFailureBlock _Nullable)failure;

/**
 Оповещение об установке оффера / Сохранить информацию об установке
 POST /offers/{id}/install?access_token=...
 
 @param offerIdString - id оффера
 @param success - success блок
 @param failure - failure блок
 */
- (void)postOfferId:(NSString * _Nullable)offerIdString
            success:(ApiSuccessBlock _Nullable)success
            failure:(ApiFailureBlock _Nullable)failure;

/**
 Оповещение об установке оффера по пакету / Сохранить информацию об установке по пакету
 POST offers/installs?access_token=...
 
 BODY
 {
 "package": "com.some.app"
 }
 
 @param offerPackageString - пакет оффера - package (AppID, BundleID)
 @param success - success блок
 @param failure - failure блок
 */
- (void)postOfferPackage:(NSString * _Nullable)offerPackageString
                 success:(ApiSuccessBlock _Nullable)success
                 failure:(ApiFailureBlock _Nullable)failure;

#pragma mark - profile payouts

- (void)getProfilePayoutsSuccess:(ApiSuccessBlock _Nullable)success
                         failure:(ApiFailureBlock _Nullable)failure;

- (void)postProfilePayoutsProvider:(ApiClientPayoutType)type
                           account:(NSString * _Nullable)account
                               sum:(NSNumber * _Nullable)sum
                           success:(ApiSuccessBlock _Nullable)success
                           failure:(ApiFailureBlock _Nullable)failure;
//NS_ASSUME_NONNULL_END

@end
