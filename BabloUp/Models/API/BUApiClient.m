//
//  BUApiClient.m
//  BabloUp
//
//  Created by Pavel Wasilenko on 10/04/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import <AdSupport/AdSupport.h>

#import "BUApiClient.h"

#import "BUOffer.h"
#import "BUPayout.h"
#import "BUUser.h"

#ifdef DEMO
//DEMO
static NSString * const apiBaseURLString = @"http://demo.babloapp.ru/api/v1/";
#else
//PROD
static NSString * const apiBaseURLString = @"http://app.babloapp.ru/api/v1/";
#endif

//DEMO TOKEN
/*
 парни, на демо сервере 20 юзеров с токенами от `demo1` до `demo20`. Офферы тоже нагенерил, на днях сделаю генерацию рефов, выполненных офферов, баланса и выплат.
 
 Пожалуйста, играйтесь теперь на демосерваке, а продакшен не будем засорять данными.
 
 Хост: demo.babloapp.ru.
 
 При желании, авторизации тоже будут работать на демосервер, но юзеры будут создаваться свежие без баланса.
 
 http://demo.babloapp.ru ( demo@mobidoo.io / demo )
 */

@implementation BUApiClient


- (instancetype)init {
    if (self = [super initWithBaseURL:[NSURL URLWithString:apiBaseURLString]]) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        self.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    
    return self;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Commom methods
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSString *)apiClientRequestTypeString:(ApiClientRequestType)type;
{
    NSString *type_string = @"TYPE_UNKNOWN";
    
    switch (type) {
        case ApiClientRequestTypeGET:
            type_string = @"GET";
            break;
        case ApiClientRequestTypePOST:
            type_string = @"POST";
            break;
        default:
            break;
    }
    
    return type_string;
}

/**
 основная сетевая функция для отправки данных
 ex param needToken BOOL необходимость отправки токена (в данный момент определяется по endpoint - отправляется во всех случаях кроме "auth"
 
 @param type тип запроса - ApiClientRequestType (~GET / ~POST )
 @param endpoint точка приложения запроса
 @param parameters NSDictionary с параметрами
 
 @param success блок успешного выполнения запроса
 @param failure блок выполнения запроса в случае ошибки
 */
- (void)requestWithType:(ApiClientRequestType)type
               endpoint:(NSString *)endpoint
             parameters:(NSDictionary *)parameters
//              needToken:(BOOL)needToken
                success:(ApiSuccessBlock)success
                failure:(ApiFailureBlock)failure;
{
    NSString *type_string = [self apiClientRequestTypeString:type];
    
    DDLogInfo(@"API - %@ - %@ - parameters\n\n%@", type_string, endpoint, parameters);
    
    NSString *endpointAndToken = endpoint;
    
    if (![endpoint isEqualToString:@"auth"]) {
        endpointAndToken = [NSString stringWithFormat:@"%@?access_token=%@", endpoint, BUDATAMANAGER.currentUser.access_token];
    }
    
    ApiSuccessBlock common_success = ^void(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        DDLogInfo(@"API - %@ - %@ - task\n\n%@\n"
                  "URL - %@", type_string, endpoint, task, task.currentRequest.URL.absoluteString);
        
        if (![endpoint hasPrefix:@"offers"]) {
            DDLogInfo(@"API - %@ - %@ - data received\n\n%@", type_string, endpoint, responseObject);
        }
        
        success(task, responseObject);
    };
    
//    __weak typeof(self) weakSelf = self;
    ApiFailureBlock common_failure = ^void(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        __strong typeof(self) strongSelf = weakSelf;
        DDLogError(@"API - %@ - %@ - task -\n\n%@\n"
                   "URL - \n%@\n", type_string, endpoint, task, task.currentRequest.URL.absoluteString);
        DDLogError(@"API - %@ - %@ - ERROR -\n\n%@\n", type_string, endpoint, error);
//        DDLogError(@"API - %@ - %@ - data received -\n\n%@\n", type_string, endpoint, error);
        
        
        //{"error":{"status":401,"message":"Unauthorized"}}
        
        NSData *jsonData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        
        NSString *jsonString =
        [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        DDLogError(@"API - %@ - %@ - ERROR - JSON -\n\n%@", type_string, endpoint, jsonString);
        
        
        NSError *e = nil;
        
        id json = nil;
        
        if (jsonData) {
            json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&e];
        }
        
        //done: error processing
        if (!e
            && json
            && [json isKindOfClass:[NSDictionary class]]
            )
        {
            NSDictionary *responseDictionary = (NSDictionary *)json;
            DDLogError(@"API - %@ - %@ - ERROR - JSON - parsing -\n\n%@",type_string, endpoint, responseDictionary);
            
            NSNumber *status = responseDictionary[@"status"];
            
            if (status
                && ![(NSNumber *)status boolValue])
            {
                //done: ствтус
                DDLogError(@"API - %@ - %@ - ERROR - status - false", type_string, endpoint);
                
                if (responseDictionary[@"error"]
                    && [responseDictionary[@"error"] isKindOfClass:[NSDictionary class]]
                    && [(NSDictionary *)responseDictionary[@"error"] valueForKey:@"message"]
                    )
                {
                    NSString *message = [(NSDictionary *)responseDictionary[@"error"] valueForKey:@"message"];
                    
                    DDLogError(@"API - %@ - %@ - ERROR - message - \n\n%@\n", type_string, endpoint, message);
                    
                    if (![task.originalRequest.URL.absoluteString containsString:@"install"]) {
                        [BUDATAMANAGER showAlertErrorWithMessage:message];
                    }
                }
            }
            
            //{"error":{"status":401,"message":"Unauthorized"}}
            if (responseDictionary[@"error"] &&
                responseDictionary[@"error"][@"status"] &&
                [responseDictionary[@"error"][@"status"] isKindOfClass:[NSNumber class]] &&
                [responseDictionary[@"error"][@"status"] isEqualToNumber:@401] &&
                [responseDictionary[@"error"][@"message"] isKindOfClass:[NSString class]] &&
                [responseDictionary[@"error"][@"message"] isEqualToString:@"Unauthorized"])
            {
                DDLogError(@"Re-Authorising user");
                [[BUDataManager instance] authorizeUserWithService:nil socialToken:nil];
            }
        }
        
        failure(task, error);
    };
    
    switch (type) {
        case ApiClientRequestTypeGET:
        {
            [self GET:endpointAndToken
            parameters:parameters
              progress:nil
               success:common_success
               failure:common_failure];
            
            break;
        }
        case ApiClientRequestTypePOST:
        {
            [self POST:endpointAndToken
            parameters:parameters
              progress:nil
               success:common_success
               failure:common_failure];
            
            break;
        }
        default:
            break;
    }
    
    
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Profile related
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 POST http://app.babloapp.ru/api/v1/auth
 
 {
 "service": "google",
 "token": "01234567890abcdef"
 }
 
 google
 facebook
 vk
 
 RESPONCE
 
 
 {
 "status": true,
 "data": {
 "first_name": "Вася",
 "last_name": "Пупкин",
 "userpic_url": "http://vasya.com/pupkin.png",
 "email": "vasya@pupkin.com",
 "balance": 47,
 "referrer_code": "sEB091",
 "referral_code": "Gf0tEs",
 "referrals_amount": 5,
 "referrals_earnings": 123.45,
 "access_token": "e4187c9f528099e0b6fb7493c6518a103b02ee681f209b008107ef44da8d3ec0"
 }
 }
 
 */
- (void)postAuth:(NSString *)token
       forService:(NSString *)service
          success:(ApiSuccessBlock)success
          failure:(ApiFailureBlock)failure;
{
    NSString *device_id = [UIDevice currentDevice].identifierForVendor.UUIDString;
    
    //TODO: замена UDID
//    if ([ASIdentifierManager sharedManager].advertisingTrackingEnabled) {
        NSUUID *ad = [ASIdentifierManager sharedManager].advertisingIdentifier;
//        if (ad && ![ad.UUIDString isEqualToString:@"00000000-0000-0000-0000-000000000000"]) {
            device_id = ad.UUIDString;
//        }
//    }
    
    
    NSMutableDictionary *parameters =
    [@{
      @"device_id"  : device_id
      } mutableCopy];
    
    if (token.length) {
        parameters[@"token"] = token;
    }
    if (service.length) {
        parameters[@"service"] = service;
    }
    
    if (BUDATAMANAGER.currentUser.access_token.length)
    {
        parameters[@"token"] = BUDATAMANAGER.currentUser.access_token;
    }
    
    [self requestWithType:ApiClientRequestTypePOST
                 endpoint:@"auth"
               parameters:parameters
     
                  success:success
                  failure:failure];
}

/**
 GET profile?access_token=
*/
- (void)getProfileSuccess:(ApiSuccessBlock)success
                  failure:(ApiFailureBlock)failure;
{
    [self requestWithType:ApiClientRequestTypeGET
                 endpoint:@"profile"
               parameters:nil
                  success:success
                  failure:failure];
}

/**
 POST referrer?access_token=
 */
- (void)postReferrer:(NSString *)referrer_code
             success:(ApiSuccessBlock)success
             failure:(ApiFailureBlock)failure;
{
    NSDictionary *parameters =
    @{
      @"referrer_code" : referrer_code
      };
    
    
    [self requestWithType:ApiClientRequestTypePOST
                 endpoint:@"referrer"
               parameters:parameters
                  success:success
                  failure:failure];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Offers related
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 Офферы / Получить список офферов (с количеством на страницу по умолчанию)

 GET offers?page=1&status=new&os=ios&access_token=
 
 @param page - NSNumber - Номер страницы. Example: 2. Integer
 
 @param status - Фильтр по статусу офферов. Possible values:  new , active , completed.
 */
- (void)getOffersPage:(NSNumber *)page
               status:(ApiClientOfferStatus)status
              success:(ApiSuccessBlock)success
              failure:(ApiFailureBlock)failure;
{
    [self getOffersPage:page
               per_page:nil
                 status:status
                success:success
                failure:failure];
}



/**
 Офферы / Получить список офферов
 
 GET offers?page=1&per_page=100&status=new&os=ios&access_token=

 @param page - номер страницы
 @param per_page - офферов на страницу
 @param status - status description
 @param success - success блок
 @param failure - failure блок
 */
- (void)getOffersPage:(NSNumber *)page
             per_page:(NSNumber *)per_page
               status:(ApiClientOfferStatus)status
              success:(ApiSuccessBlock)success
              failure:(ApiFailureBlock)failure;
{
    NSMutableDictionary *parameters =
    [@{
       @"os"         : @"ios",
      } mutableCopy];
    
    NSString *offerStatus = [BUOffer offerStatusFromEnum:status];
    
    if (offerStatus.length) {
        parameters[@"status"] = offerStatus;
    }
    
    if (page.integerValue) {
        parameters[@"page"] = page;
    }
    if (per_page.integerValue) {
        parameters[@"per_page"] = per_page;
    } else {
        parameters[@"per_page"] = DEFAULT_PER_PAGE;
    }
    
    [self requestWithType:ApiClientRequestTypeGET
                 endpoint:@"offers"
               parameters:[parameters copy]
                  success:success
                  failure:failure];
}

/**
 Оповещение об установке оффера / Сохранить информацию об установке 
 POST /offers/{id}/installs?access_token=...
 
 @param offerIdString - id оффера
 @param success - success блок
 @param failure - failure блок
 */
- (void)postOfferId:(NSString *)offerIdString
            success:(ApiSuccessBlock)success
            failure:(ApiFailureBlock)failure;
{
    NSString *endpoint = [NSString stringWithFormat:@"offers/%@/installs", offerIdString];
    
    [self requestWithType:ApiClientRequestTypePOST
                 endpoint:endpoint
               parameters:nil
                  success:success
                  failure:failure];
}

/**
 Оповещение об установке оффера по пакету / Сохранить информацию об установке по пакету 
 POST offers/installs?access_token=...
 
 BODY
 {
 "package": "com.some.app"
 }
 
 @param offerPackageString - пакет оффера - package (AppID, BundleID)
 @param success - success блок
 @param failure - failure блок
 */
- (void)postOfferPackage:(NSString *)offerPackageString
            success:(ApiSuccessBlock)success
            failure:(ApiFailureBlock)failure;
{
    NSDictionary *parameters = nil;
    
    if (offerPackageString.length) {
        parameters = @{ @"package" : offerPackageString };
    }
        
    [self requestWithType:ApiClientRequestTypePOST
                 endpoint:@"offers/installs"
               parameters:parameters
                  success:success
                  failure:failure];
}

#pragma mark - profile payouts

- (void)getProfilePayoutsSuccess:(ApiSuccessBlock)success
                         failure:(ApiFailureBlock)failure;
{
    [self requestWithType:ApiClientRequestTypeGET
                 endpoint:@"profile/payouts"
               parameters:nil
                  success:success
                  failure:failure];
}

/**
POST profile/payouts
 
 provider
 required
 enum
 Провайдер, на счет которого поступит выплата.
 webmoney / qiwi / phone
 
 phone
 required
 string
 Номер телефона на стороне провайдера, куда поступят денежные средства.
 9201234567
 
 account
 string
 Счет на стороне провайдера, куда поступят денежные средства.
 1234567887654321
 
 amount
 required
 number
 Количество монет для вывода.
 500
 
 {
 "provider": "webmoney",
 "phone": "9201234567",
 "account": "1234567887654321",
 "amount": 500
 }
 
 */

- (void)postProfilePayoutsProvider:(ApiClientPayoutType)type
                            account:(NSString *)account
                               sum:(NSNumber *)sum
                           success:(ApiSuccessBlock)success
                           failure:(ApiFailureBlock)failure;
{
    NSString *typeString = [BUPayout stringPayoutType:type];
    
    NSDictionary *parameters =
    @{
      @"provider"   : typeString,
      @"account"    : account,
      @"amount"     : sum
      };
    
    
    [self requestWithType:ApiClientRequestTypePOST
                 endpoint:@"profile/payouts"
               parameters:parameters
                  success:success
                  failure:failure];
}

@end
