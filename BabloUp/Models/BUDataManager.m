//
//  BUDataManager.m
//  BabloUp
//
//  Created by Pavel Wasilenko on 06/04/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import "BUDataManager.h"

#import "BUOffer.h"
#import "BUPayout.h"

#import <YapDatabase/YapDatabaseView.h>
#import <YapDatabase/YapDatabaseViewMappings.h>
#import <YapDatabase/YapDatabaseFilteredView.h>

//@import MobileCoreServices;

@interface BUDataManager ()
{
    NSString *_token;
    
    NSNumber *current_page;
    NSNumber *per_page;
    NSNumber *total_elements;
    
//    YapDatabaseViewMappings *_offersMappings;
    
//    YapDatabaseFilteredView *_filteredOffers;
    
//    __block NSArray *activeTasks;
    
//    NSArray *_isLoadingOffersArray;
//    NSArray *_loadedOffersDateArray;
}

@property (strong) NSMutableArray<NSNumber *>   *isLoadingOffersArray;
@property (strong) NSMutableArray<NSDate *>     *loadedOffersDateArray;

@property (strong) NSNumber *shouldLoadCompleted;

@end

@implementation BUDataManager

@synthesize database = _database;

@synthesize bgConnection = _bgConnection;
@synthesize uiConnection = _uiConnection;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - singltone
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

+ (instancetype)instance {
    static BUDataManager *sharedInstance = nil;
    static dispatch_once_t dataManagerOnceToken;
    dispatch_once(&dataManagerOnceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    if (self = [super init])
    {        
        self.api = [[BUApiClient alloc] init];
        self.router = [[BURouterController alloc] init];
        
        //BUUser
        self.currentUser = [[BUUser alloc] init];
        
//        //VK SDK
//        self.vkSdkInstance = [VKSdk initializeWithAppId:VK_APP_ID];
//        [self.vkSdkInstance setUiDelegate:self.router];
//        [self.vkSdkInstance registerDelegate:self];
        
        [self createAndSetupDatabase];
        
        current_page = @1;
        
        //
        NSDate *ts = [NSDate dateWithTimeIntervalSinceNow:-86400.0];
        
        //для мониторинга статуса загрузки типов офферов (new / active / completed)
        _isLoadingOffersArray = [@[ @NO, @NO, @NO ] mutableCopy];
        
        //для мониторинга времени загрузки типов офферов
        _loadedOffersDateArray = [@[ ts, ts, ts ] mutableCopy];
        
        _shouldLoadCompleted = @NO;
        
        
    }
    return self;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Database
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSString *)databasePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *baseDir = ([paths count] > 0) ? paths[0] : NSTemporaryDirectory();
    
    NSString *databaseName = @"database.sqlite";
    
    NSString *dbPath = [baseDir stringByAppendingPathComponent:databaseName];
    
    DDLogInfo(@"YapDatabase - Database path:\n%@", dbPath);
    
    return dbPath;
}

- (void)createAndSetupDatabase;
{
    _database = [self database];
    [self setupDatabase];
}

- (void)initializeMappings
{
    if (!BUDATAMANAGER.orderMappings) {
        // What the heck are mappings?
        // https://github.com/yaptv/YapDatabase/wiki/Views#mappings
        
        BUDATAMANAGER.orderMappings   = [[YapDatabaseViewMappings alloc] initWithGroups:@[ @"offer-new", @"offer-active", @"offer-completed" ] view:@"offer-order"];
        
        [BUDATAMANAGER.uiConnection readWithBlock:^(YapDatabaseReadTransaction *transaction) {
            // One time initialization
            [BUDATAMANAGER.orderMappings updateWithTransaction:transaction];
        }];
    }
    
    if (!BUDATAMANAGER.payMappings) {
        // What the heck are mappings?
        // https://github.com/yaptv/YapDatabase/wiki/Views#mappings
        
        BUDATAMANAGER.payMappings   = [[YapDatabaseViewMappings alloc] initWithGroups:@[ @"payout-group" ] view:@"payout-order"];
        
        [BUDATAMANAGER.uiConnection readWithBlock:^(YapDatabaseReadTransaction *transaction) {
            // One time initialization
            [BUDATAMANAGER.payMappings updateWithTransaction:transaction];
        }];
    }
    
}

- (void)setupDatabase
{
//    NSString *databasePath = [self databasePath];
    
    //    [[NSFileManager defaultManager] removeItemAtPath:databasePath error:NULL];
    
    // Create the database.
    // We do this using the default settings.
    // If you want to get fancy, you can do things like customize the serialization/deserialization routines.
    
    DDLogVerbose(@"YapDatabase - Creating database instance...");
    
    
    // Create the main view.
    // This is a database extension that will sort our objects in the manner in which we want them.
    //
    // For more information on views, see the wiki article:
    // https://github.com/yaptv/YapDatabase/wiki/Views
    
    DDLogVerbose(@"YapDatabase - Creating offer view...");
    
    YapDatabaseViewGrouping *groupingOffer = [YapDatabaseViewGrouping withRowBlock:^NSString * _Nullable(YapDatabaseReadTransaction * _Nonnull transaction, NSString * _Nonnull collection, NSString * _Nonnull key, id  _Nonnull object, id  _Nullable metadata)
                                              {
                                                  // The grouping block is used to:
                                                  // - filter items that we don't want in the view
                                                  // - place items into specific groups (which can be used as sections in a tableView, for example)
                                                  
                                                  NSString *group = nil;
                                                  
                                                  if ([object isKindOfClass:[BUOffer class]]) {
                                                      BUOffer *off = (BUOffer *)object;
//#warning DELETE THIS - for test only!
//                                                      if (!off.package.length) { return group; }
                                                      
                                                      switch (off.statusEnum) {
                                                          case ApiClientOfferStatusNew:
                                                              group = @"offer-new";
                                                              break;
                                                          case ApiClientOfferStatusActive:
                                                              group = @"offer-active";
                                                              break;
                                                          case ApiClientOfferStatusCompleted:
                                                              group = @"offer-completed";
                                                              break;
                                                          default:
                                                              break;
                                                      }
                                                  }
                                                  
                                                  
                                                  return group;
                                              }];
    
    YapDatabaseViewSorting *sortingOffer = [YapDatabaseViewSorting withObjectBlock:
                                            ^NSComparisonResult(YapDatabaseReadTransaction *transaction, NSString *group,
                                                                NSString *collection1, NSString *key1, id object1,
                                                                NSString *collection2, NSString *key2, id object2)
                                            {
                                                // The sorting block is used to sort items within their group/section.
                                                
                                                __unsafe_unretained BUOffer *offer1 = (BUOffer *)object1;
                                                __unsafe_unretained BUOffer *offer2 = (BUOffer *)object2;
                                                
//                                                return [offer1.created_at compare:offer2.created_at options:NSLiteralSearch];
                                                
                                                NSComparisonResult order = [offer1.offer_id compare:offer2.offer_id];
                                                
                                                return order;
                                            }];
    
    NSSet *whitelistOffer = [NSSet setWithObjects:@"offer", nil];
    
    YapDatabaseViewOptions *optionsOffer = [[YapDatabaseViewOptions alloc] init];
    optionsOffer.allowedCollections = [[YapWhitelistBlacklist alloc] initWithWhitelist:whitelistOffer];

    
    
    YapDatabaseAutoView *viewOfferOrder = [[YapDatabaseAutoView alloc] initWithGrouping:groupingOffer
                                                                                sorting:sortingOffer
                                                                             versionTag:@"1"
                                                                                options:optionsOffer];
    
    if (![self.database registerExtension:viewOfferOrder withName:@"offer-order"])
    {
        DDLogError(@"YapDatabase - Unable to register extension: offer-order!!");
    } else {
        DDLogVerbose(@"YapDatabase - Registered extension offer-order view.");
    }
    
    YapDatabaseViewGrouping *groupingPayout = [YapDatabaseViewGrouping withRowBlock:^NSString * _Nullable(YapDatabaseReadTransaction * _Nonnull transaction, NSString * _Nonnull collection, NSString * _Nonnull key, id  _Nonnull object, id  _Nullable metadata)
                                               {
                                                   // The grouping block is used to:
                                                   // - filter items that we don't want in the view
                                                   // - place items into specific groups (which can be used as sections in a tableView, for example)
                                                   
                                                   
                                                   if ([object isKindOfClass:[BUPayout class]])
                                                   return @"payout-group";
                                                   
                                                   return nil;
                                               }];
    
    YapDatabaseViewSorting *sortingPayout = [YapDatabaseViewSorting withObjectBlock:
                                             ^NSComparisonResult(YapDatabaseReadTransaction *transaction, NSString *group,
                                                                 NSString *collection1, NSString *key1, id object1,
                                                                 NSString *collection2, NSString *key2, id object2)
                                             {
                                                 // The sorting block is used to sort items within their group/section.
                                                 
                                                 __unsafe_unretained BUPayout *pay1 = (BUPayout *)object1;
                                                 __unsafe_unretained BUPayout *pay2 = (BUPayout *)object2;
                                                 
                                                 return [pay1.created_at compare:pay2.created_at options:NSLiteralSearch];
                                             }];
    
    NSSet *whitelistPayout = [NSSet setWithObjects:@"payout", nil];
    
    YapDatabaseViewOptions *optionsPayout = [[YapDatabaseViewOptions alloc] init];
    optionsPayout.allowedCollections = [[YapWhitelistBlacklist alloc] initWithWhitelist:whitelistPayout];
    
    YapDatabaseAutoView *viewPayoutOrder = [[YapDatabaseAutoView alloc] initWithGrouping:groupingPayout
                                                                                 sorting:sortingPayout
                                                                              versionTag:@"1"
                                                                                 options:optionsPayout];
    
    if (![self.database registerExtension:viewPayoutOrder withName:@"payout-order"])
    {
        DDLogError(@"YapDatabase - Unable to register extension: payout-order!!");
    } else {
        DDLogVerbose(@"YapDatabase - Registered extension payout-order view.");
    }
    
    
    
    //    // Create the Full Text Search (FTS) index.
    //    // This is a database extension that allows us to perform extremely fast text based searches.
    //    //
    //    // The FTS extension is built atop the FTS module within sqlite (which was originally written by Google).
    //    //
    //    // For more information on views, see the wiki article:
    //    // https://github.com/yaptv/YapDatabase/wiki/Full-Text-Search
    //
    //    DDLogVerbose(@"Creating fts...");
    //
    //    YapDatabaseFullTextSearchHandler *handler = [YapDatabaseFullTextSearchHandler withObjectBlock:^(NSMutableDictionary *dict, NSString *collection, NSString *key, id object) {
    //        __unsafe_unretained Person *person = (Person *)object;
    //        dict[@"name"] = person.name;
    //    }];
    //
    //    YapDatabaseFullTextSearch *fts = [[YapDatabaseFullTextSearch alloc] initWithColumnNames:@[ @"name" ] handler:handler versionTag:@"1"];
    //
    //
    //    if (![database registerExtension:fts withName:@"fts"])
    //    {
    //        DDLogError(@"Unable to register extension: fts");
    //    }
    
    //    // Create the search view.
    //    // This extension allows you to use an existing FTS extension, perform searches on it,
    //    // and then pipe the search results into a regular view.
    //    //
    //    // There are a couple ways we can set this up:
    //    // - Use the FTS module to search an existing view
    //    // - Just use the FTS module, and provide a groupingBlock/sortingBlock to order the results
    //    //
    //    // In our case, we want to use the FTS module in order to search the main view.
    //    // So we're going to setup the search view accordingly.
    //
    //    YapDatabaseSearchResultsViewOptions *searchViewOptions = [[YapDatabaseSearchResultsViewOptions alloc] init];
    //    searchViewOptions.isPersistent = NO;
    //
    //    YapDatabaseSearchResultsView *searchResultsView = [[YapDatabaseSearchResultsView alloc] initWithFullTextSearchName:@"fts"
    //                                                                                                        parentViewName:@"order"
    //                                                                                                            versionTag:@"1"
    //                                                                                                               options:searchViewOptions];
    //    
    //    if (![database registerExtension:searchResultsView withName:@"searchResults"])
    //    {
    //        DDLogError(@"Unable to register extension: searchResults");
    //    }
    
}


//- (void)startActiveOffersTracking;
//{
//    //OFFER-ACTIVE processing part
//    if (_orderMappings == nil) {
////        [self.offerConnection beginLongLivedReadTransaction];
//        [self initializeMappings];
////        [[NSNotificationCenter defaultCenter] addObserver:self
////                                                 selector:@selector(databaseModified:)
////                                                     name:YapDatabaseModifiedNotification
////                                                   object:BUDATAMANAGER.database];
//    }
//}


/*
-(void)databaseModified:(NSNotification *)notification {
    
    
    NSArray *notifications = [self.uiConnection beginLongLivedReadTransaction];
    
    if ([notifications count] == 0) {
        return;
    }
    
    NSArray *rowChanges = nil;
    
    DDLogVerbose(@"ACTIVE OFFERS - Calculating rowChanges for ACTIVE OFFERS...");
    
    NSString *extension = @"offer-order"; //@"offer-order-new";

    
    DDLogVerbose(@"ACTIVE OFFERS - Extension = %@", extension);
    
    [[BUDATAMANAGER.uiConnection ext:extension] getSectionChanges:NULL
                                                       rowChanges:&rowChanges
                                                 forNotifications:notifications
                                                     withMappings:_orderMappings];
    
    DDLogVerbose(@"ACTIVE OFFERS - Processing rowChanges for ACTIVE OFFERS...");
    
    if ([rowChanges count] > 0)
    {
        DDLogVerbose(@"ACTIVE OFFERS - rowChanges count = %li...", (long)[rowChanges count]);
        
        for (YapDatabaseViewRowChange *rowChange in rowChanges)
        {
            DDLogVerbose(@"ACTIVE OFFERS - rowChange = \n\n%@\n", rowChange);
            
            NSString *originalGroup = rowChange.originalGroup;
            NSString *finalGroup = rowChange.finalGroup;
            
            DDLogVerbose(@"ACTIVE OFFERS - from group '%@' to group '%@'", originalGroup, finalGroup);
            
            NSInteger row = rowChange.indexPath.row;
            NSInteger newRow = rowChange.newIndexPath.row;
            
            switch (rowChange.type)
            {
                case YapDatabaseViewChangeDelete :
                {
                    DDLogVerbose(@"ACTIVE OFFERS - rowChanges delete %li..., ", row);
                    //удалено из active. скорее всего игнорим.
                    
                    break;
                }
                case YapDatabaseViewChangeInsert :
                {
                    DDLogVerbose(@"ACTIVE OFFERS - rowChanges insert %li...", newRow);
                    //добавлено в active. проверяем.
                    [self checkOfferAtIndex:newRow];
                    
                    break;
                }
                case YapDatabaseViewChangeMove :
                {
                    DDLogVerbose(@"ACTIVE OFFERS - rowChanges move %li to %li ...", row, newRow);
                    //перемещение = поменялся индекс. х.з. скорее всего проверим.
                    
                    [self checkOfferAtIndex:row];
                    [self checkOfferAtIndex:newRow];
                    
                    //                    [_tableView moveRowAtIndexPath:indexPath
                    //                                       toIndexPath:newIndexPath];
//                    [fromTable deleteRowsAtIndexPaths:@[ indexPath ]
//                                     withRowAnimation:UITableViewRowAnimationAutomatic];
//                    
//                    [toTable insertRowsAtIndexPaths:@[ newIndexPath ]
//                                   withRowAnimation:UITableViewRowAnimationAutomatic];
                    
                    
                    
                    
                    break;
                }
                case YapDatabaseViewChangeUpdate :
                {
                    DDLogVerbose(@"ACTIVE OFFERS - rowChanges update %li...", row);
                    //проверим
                    
                    [self checkOfferAtIndex:row];
                    
//                    [fromTable reloadRowsAtIndexPaths:@[ indexPath ]
//                                     withRowAnimation:UITableViewRowAnimationNone];
//                    
//                    ???
//                    if (fromTable != toTable)
//                        [toTable reloadRowsAtIndexPaths:@[ indexPath ]
//                                       withRowAnimation:UITableViewRowAnimationNone];
                    
                    break;
                }
            }

        }
        
        //инкриментальный апдейт прошёл, можно запрашивать Completed
        
        [self loadOffersOfType:ApiClientOfferStatusCompleted];
    }
}
*/

- (YapDatabase *)database;
{
    if (_database == nil) {
        //        @synchronized (self) {
        YapDatabase *db = [[YapDatabase alloc] initWithPath:[self databasePath]];
        _database = db;
        //        };
    }
    
    return _database;
}

- (YapDatabaseConnection *)bgConnection;
{
    if (_bgConnection == nil) {
        //        @synchronized (self) {
        YapDatabaseConnection *bg = [self.database newConnection];
        _bgConnection = bg;
        //        };
    }
    
    return _bgConnection;
}

- (YapDatabaseConnection *)uiConnection;
{
    if (_uiConnection == nil) {
        //        @synchronized (self.da) {
        YapDatabaseConnection *ui = [self.database newConnection];
        ui.objectCacheLimit = 1000;
        ui.metadataCacheEnabled = NO;
        _uiConnection = ui;
        //        };
    }
    
    return _uiConnection;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//#pragma mark - vkSdkDelegate
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result {
//    if (result.token)
//    {
//        //TODO: Api - отправка токена соцсети (VK)
//        
//        [self authorizeUserWithService:@"vk"
//                           socialToken:result.token.accessToken];
//    }
//}
//
//- (void)vkSdkUserAuthorizationFailed {
//    DDLogError(@"vkSdkUserAuthorizationFailed");
//}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Message/Error related
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message;
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok =
    [UIAlertAction actionWithTitle:@"Ok"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * _Nonnull action) {
                               ;
                           }];
    [alert addAction:ok];
    
    //done: router - current view controller
    [self.router.navigationController.topViewController presentViewController:alert animated:YES completion:nil];
}

- (void)showAlertErrorWithMessage:(NSString *)message;
{
    [self showAlertWithTitle:@"Ошибка!" message:message];
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Token
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSString *)token {
    _token = self.currentUser.access_token;
    
    //[[NSUserDefaults standardUserDefaults] objectForKey:API_TOKEN_KEYPATH];
    
    return _token;
}

- (void)setToken:(NSString *)token {
    self.currentUser.access_token = token;
    [self.currentUser save];
}


- (BOOL)isTokenStored {
    BOOL isStored = NO;
    
    if ([self token] && [[self token] length]) {
        isStored = YES;
    }
    
    return isStored;
}

- (void)logout;
{
    //done: очистка User
    [self.currentUser clear];
    
    
    //done: очистка YapDatabase
    
    /*
     https://github.com/yapstudios/YapDatabase/issues/139
     
     extern NSString *const YapDatabaseClosedNotification;
     
     extern NSString *const YapDatabasePathKey;
     extern NSString *const YapDatabasePathWalKey;
     extern NSString *const YapDatabasePathShmKey;
     
     
     * Therefore the best approach is to do the following:
     * - destroy your YapDatabase instance (set it to nil)
     * - destroy all YapDatabaseConnection instances
     * - wait for YapDatabaseClosedNotification
     * - use notification as hook to delete all associated sqlite files from disk
     
     */
    
    _database = nil;
    _uiConnection = nil;
    _bgConnection = nil;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deleteDatabaseFiles:)
                                                 name:YapDatabaseClosedNotification
                                               object:nil];
    
    [self.router showInitialViewController];
}

- (void)deleteDatabaseFiles:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:YapDatabaseClosedNotification object:nil];
    
    /*
     NSDictionary *userInfo = @{
     YapDatabasePathKey    : self.databasePath     ?: @"",
     YapDatabasePathWalKey : self.databasePath_wal ?: @"",
     YapDatabasePathShmKey : self.databasePath_shm ?: @""
     };
     */
    
    NSDictionary *userInfo = notification.userInfo;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSError *error;
    
    NSMutableArray * yapFilesToDelete = [NSMutableArray array];
    
    if (userInfo[YapDatabasePathKey]) {
        [yapFilesToDelete addObject:userInfo[YapDatabasePathKey]];
    }
    if (userInfo[YapDatabasePathWalKey]) {
        [yapFilesToDelete addObject:userInfo[YapDatabasePathWalKey]];
    }
    if (userInfo[YapDatabasePathShmKey]) {
        [yapFilesToDelete addObject:userInfo[YapDatabasePathShmKey]];
    }
    
    for(NSString *str in yapFilesToDelete) {
        BOOL success = [fileManager removeItemAtPath:str error:&error];
    
        if (success && !error) {
            DDLogInfo(@"LOGOUT - Successfully removed : \n\n%@\n", str);
        }
        else
        {
            DDLogError(@"LOGOUT - delete file ERROR: \n\n%@\n", error);
            DDLogError(@"LOGOUT - Could not delete file :\n\n%@\n", str);
        }
    }
    
    
    
    [self createAndSetupDatabase];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - User related
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)authorizeUserWithService:(NSString *)service
                     socialToken:(NSString *)token;
{
    
    [self.api postAuth:token
            forService:service
               success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                   DDLogInfo(@"DataManager - postToken OK");
                   if ([self.currentUser parseResponce:responseObject
                                     forService:service
                                      withToken:token])
                   {
                       [self.currentUser save];
                       [[NSNotificationCenter defaultCenter] postNotificationName:USER_UPDATED_NOTIFICATION object:nil];
                       
                       //в залогиненное состояние.
                       [self.router showOffers];
                   }
               } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
                   DDLogError(@"DataManager - postToken ERROR");
               }];
}

- (void)updateUserFromResponce:(NSDictionary *)userdic;
{
    [self.currentUser parseProfileDictionary:userdic];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:USER_UPDATED_NOTIFICATION object:nil];
}

- (void)updateUser;
{
    if (self.isTokenStored)
    {
        [self.api getProfileSuccess:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
        {
            [self.currentUser parseResponce:responseObject forService:nil withToken:nil];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:USER_UPDATED_NOTIFICATION object:nil];
        }
        
                            failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error)
        {
            ;
        }];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Offers
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



- (void)loadOffersOfType:(ApiClientOfferStatus)type;
{
    if ([[BUDataManager instance] isTokenStored] &
        [[BUDataManager instance] shouldLoadOffersOfType:type])
    {
        NSNumber *page = @1;
        
        [self loadOffersOfType:type page:page];
    }

}

- (void)loadOffersOfType:(ApiClientOfferStatus)type
                    page:(NSNumber *)page;
{
    if (page.integerValue == 1 && self.isLoadingOffersArray[type].boolValue) {
        return;
    }
    
    [self startLoadingOffersOfType:type];
    
    [self.api getOffersPage:page
                     status:type
                    success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
    {
        if ([BUOffer parseResponce:responseObject])
        {
            if ([self parseMetaDataAndSouldContinue:responseObject]) {
                [self loadOffersOfType:type page:@(page.integerValue + 1)];
            } else {
                //done: обновить timestamp, isLoading = NO, нотификейшн
                [self endedLoadingOfferOfType:type];
            }
        } else {
            [self failedLoadingOfferOfType:type];
        }
    }
                    failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error)
    {
        [self failedLoadingOfferOfType:type];
        ;
    }];
}


/**
 Определяет, есть ли следующие страницы для загрузки
 
 @param responseObject - id (NSDictionary) то, что получено от AFNetworking
 @return - есть ли следующие странцы для загрузки
 */
- (BOOL)parseMetaDataAndSouldContinue:(id _Nullable)responseObject;
{
    BOOL shouldContinue = NO;
    
    //    responseObject;
    /*
     meta =     {
     page = 1;
     "per_page" = 500;
     "total_elements" = 736;
     };
     status = 1;
     }
     */
    
    NSNumber *meta_page = @1;
    
    NSNumber *meta_per_page = DEFAULT_PER_PAGE;
    
    NSNumber *meta_total_elements = @0;
    
    if (responseObject
        && [responseObject isKindOfClass:[NSDictionary class]])
    {
        NSDictionary *responseDictionary = (NSDictionary *)responseObject;
        DDLogInfo(@"get offers - get - checking meta -\n\n%@",responseDictionary[@"meta"]);
        NSNumber *status = responseDictionary[@"status"];
        
        if (status
            && [(NSNumber *)status boolValue])
        {
            if (responseDictionary[@"meta"] && [responseDictionary[@"meta"] isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary * meta = (NSDictionary *)responseDictionary[@"meta"];
                
                if (meta[@"page"] && [meta[@"page"] isKindOfClass:[NSNumber class]]) {
                    meta_page = (NSNumber *)meta[@"page"];
                }
                
                if (meta[@"per_page"] && [meta[@"per_page"] isKindOfClass:[NSNumber class]]) {
                    meta_per_page = (NSNumber *)meta[@"per_page"];
                }
                
                if (meta[@"total_elements"] && [meta[@"total_elements"] isKindOfClass:[NSNumber class]]) {
                    meta_total_elements = (NSNumber *)meta[@"total_elements"];
                }
                
                per_page = meta_per_page;
                total_elements = meta_total_elements;
                
                if (meta_total_elements.integerValue > (per_page.integerValue * meta_page.integerValue)) {
                    current_page = @(current_page.integerValue + 1);
                    shouldContinue = YES;
                }
                
            }
        }
        
    }
    
    return shouldContinue;
}


///helper+status-triggering methods


- (void)notifiyOfferCompletedMade;
{
    self.shouldLoadCompleted = @YES;
}

- (void)startLoadingOffersOfType:(ApiClientOfferStatus)type;
{
    self.isLoadingOffersArray[type] = @YES;
    
    //done: Don't start first page load if self.isLoadingOffersArray[type] == @YES
}

- (void)failedLoadingOfferOfType:(ApiClientOfferStatus)type;
{
    self.isLoadingOffersArray[type] = @NO;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:OFFERS_LOADED_NOTIFICATION object:nil userInfo:@{@"type" : @(type), @"success" : @NO }];
}

- (void)endedLoadingOfferOfType:(ApiClientOfferStatus)type;
{
    self.isLoadingOffersArray[type] = @NO;
    
    self.loadedOffersDateArray[type] = [NSDate date];
    
    //TODO: notification
    
    [[NSNotificationCenter defaultCenter] postNotificationName:OFFERS_LOADED_NOTIFICATION object:nil userInfo:@{@"type" : @(type), @"success" : @YES}];
    
    //TODO: 1) receive notification. if type = @1 (active) - start checking offers. on the end of checking - start loading type = Completed
    
    switch (type) {
        case ApiClientOfferStatusNew:
        {
//            [[BUDataManager instance] startActiveOffersTracking];
            //should load Active
            [[BUDataManager instance] loadOffersOfType:ApiClientOfferStatusActive];
            
            break;
        }
        case ApiClientOfferStatusActive:
        {
            //TODO: should check for completion ? скорее всего инкрименталкой добъётся
            [BUDATAMANAGER processAllActiveOffers];
            break;
        }
        default:
            break;
    }
}

- (BOOL)shouldLoadOffersOfType:(ApiClientOfferStatus)type;
{
    BOOL should = NO;
    
    if (![self.isLoadingOffersArray[type] boolValue]) {
        NSDate *ts = self.loadedOffersDateArray[type];
        
        should = ( fabs(ts.timeIntervalSinceNow) > 60.0);
    }
    
    if (!should) {
        [[NSNotificationCenter defaultCenter] postNotificationName:OFFERS_LOADED_NOTIFICATION object:nil userInfo:@{@"type" : @(type), @"success" : @NO }];
    }
    
    return should;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Offers processing
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 Оповещение к API об установке оффера / Сохранить информацию об установке по пакету POST /offers/installs?access_token=...
 
 BODY
 {
 "package": "com.some.app"
 }
 
 @param offer - оффер, откуда берётся строка с offer.packet (BundleID - AppID)
 
 */
- (void)notifyOfferCompleted:(BUOffer *)offer
{
    //done: Do I need success / fallback here ? NO
    
    if (offer.package.length) {
        [self.api postOfferPackage:offer.package
                           success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
         {
             //TODO: do something if everything is Ok ?
             [[BUDataManager instance] notifiyOfferCompletedMade];
         }
                           failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error)
         {
             ;
         }];
    }
}

//Table emulation

- (NSInteger)numberOfActiveOffers;
{
    NSUInteger count = 0;
    
    count = [_orderMappings numberOfItemsInGroup:@"offer-active"];
    
    DDLogInfo(@"numberOfActiveOffers = %li", (unsigned long)count);
    
    return count;
}

- (void)checkOfferAtIndex:(NSInteger)index;
{
//    __block BUOffer *offer = nil;
    
    __weak typeof(self) weakSelf = self;
    [[BUDataManager instance].uiConnection readWithBlock:^(YapDatabaseReadTransaction *transaction) {
        BUOffer *offer = [[transaction ext:@"offer-order"] objectAtIndex:index inGroup:@"offer-active"];;
        
        __strong typeof(self) strongSelf = weakSelf;
        
        [strongSelf checkIfOfferCompleted:offer sendCompletion:YES];
    }];
    
    
}

- (void)processAllActiveOffers;
{
    NSInteger count = [self numberOfActiveOffers];
    
    for (NSInteger i = 0; i < count; i++) {
        [self checkOfferAtIndex:i];
    }
    
    //done: перебрано - можно загрузить Completed
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        __strong typeof(self) strongSelf = weakSelf;
        
        [strongSelf loadOffersOfType:ApiClientOfferStatusCompleted];
        [strongSelf updateUser];
    });
}


/**
 Check if the offer is completed

 @param offer - Offer to check for completion
 @param sendComplete - should send information about completion to a server
 @return if the Offer is
 */
- (BOOL)checkIfOfferCompleted:(BUOffer *)offer sendCompletion:(BOOL)sendComplete;
{
    BOOL completed = NO;
    
    if (!offer.package.length)
    {
//        DDLogInfo(@"offer '%@' have no package", offer.offer_id_str);
        return completed;
    } else {
//        DDLogInfo(@"offer '%@' HAVE package", offer.offer_id_str);
    }
    
    Class proxyClass = NSClassFromString([[NSString alloc] initWithData: [[NSData alloc] initWithBase64EncodedString:@"TFNBcHBsaWNhdGlvblByb3h5" options:0] encoding:NSUTF8StringEncoding]); //(@"LSApplicationProxy");
    
    SEL appProxySel = NSSelectorFromString([[NSString alloc] initWithData:[[NSData alloc] initWithBase64EncodedString:@"YXBwbGljYXRpb25Qcm94eUZvcklkZW50aWZpZXI6" options:0] encoding:NSUTF8StringEncoding]); // (@"applicationProxyForIdentifier:");
        
    BOOL responds = [proxyClass respondsToSelector:appProxySel];
    if (responds) {
        
        id appProxyInstance = [proxyClass performSelector:appProxySel withObject:offer.package];
        
        NSString *act = [[NSString alloc] initWithData:[[NSData alloc] initWithBase64EncodedString:@"aXNJbnN0YWxsZWQ=" options:0] encoding:NSUTF8StringEncoding];
                                                        
//                                                        @"aXNJbnN0YWxsZWQ="
//
//        NSData *stringData = [@"isInstalled" dataUsingEncoding:NSUTF8StringEncoding];
//        NSString *base64String = [stringData base64EncodedStringWithOptions:0];
//        NSLog(@"%@", base64String); // aXNJbnN0YWxsZWQ=
        
        id val = [appProxyInstance valueForKey:act];
        
        
        if (val && [val respondsToSelector:@selector(boolValue)]) {
            
//            NSString *is = [val boolValue] ? @"INSTALLED" : @"NOT INSTALLED";
//            DDLogInfo(@"'%@' isInstalled = %@", offer.package, is);
//            NSString *msg = [NSString stringWithFormat:@"App with BundleID '%@'\n is %@", package, is];
//            [[BUDataManager instance] showAlertWithTitle:@"Checked Bundle" message:msg];
        
            //TODO: send API request if completed and val = YES
            if (sendComplete && [val boolValue]) {
                [[BUDataManager instance].api postOfferPackage:offer.package
                                                       success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
                {
                    ;
                    //TODO: добавить триггер, что надо обновить Completed... или не надо, обновлять в любом случае по завершении
                    
                    DDLogVerbose(@"OFFER - sent confirmation");
                    
                }
                                                       failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error)
                {
                    ;
                                                        
                }];
            }
        }
    }
    
    return completed;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Payments
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)loadCoins;
{
    [self.api getProfilePayoutsSuccess:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
    {
        [BUPayout parseResponce:responseObject];
    }
                               failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error)
    {
        ;
    }];
}

/*
 
 {
 "status": true,
 "data": {
 
 "payout": {
 "provider": "webmoney",
 "account": "1234567887654321",
 "amount": 500,
 "created_at": "2017-05-02 14:54:01",
 "status": "completed"
 },
 
 "profile": {
 "first_name": "Вася",
 "last_name": "Пупкин",
 "userpic_url": "http://vasya.com/pupkin.png",
 "email": "vasya@pupkin.com",
 "balance": 47,
 "referrer_code": "sEB091",
 "referral_code": "Gf0tEs",
 "referrals_amount": 5,
 "referrals_earnings": 123.45,
 "access_token": "e4187c9f528099e0b6fb7493c6518a103b02ee681f209b008107ef44da8d3ec0"
 }
 }
 }
 */

- (void)createPaymentOfType:(ApiClientPayoutType)type
                     amount:(NSNumber *)amount
                    account:(NSString *)account;
{
    [self.api postProfilePayoutsProvider:type
                                 account:account
                                     sum:amount
                                 success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
    {
        //done: Responce analysis
        if (responseObject
            && [responseObject isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *responseDictionary = (NSDictionary *)responseObject;
            DDLogInfo(@"POST profile/payout - checking resonse -\n\n%@",responseDictionary);
            NSNumber *status = responseDictionary[@"status"];
            
            if (status
                && [(NSNumber *)status boolValue])
            {
                //TODO: Parse Payout
                if (
                    responseDictionary[@"data"]
                    && [responseDictionary[@"data"] isKindOfClass:[NSDictionary class]])
                {
                    if (
                        responseDictionary[@"data"][@"payout"]
                        && [responseDictionary[@"data"][@"payout"] isKindOfClass:[NSDictionary class]]
                        )
                    {
                        NSDictionary *payoutDic = [(NSDictionary *)responseDictionary[@"data"] objectForKey:@"payout"];
                        BUPayout *pay = [BUPayout parseFromDictionary:payoutDic];
                        
                        DDLogInfo(@"Payout - \n\n%@\n", pay);
                        
                        [BUDATAMANAGER.bgConnection  asyncReadWriteWithBlock:^(YapDatabaseReadWriteTransaction * _Nonnull transaction) {
                            [transaction setObject:pay forKey:pay.generatedIndex inCollection:@"payout"];
                            
                            DDLogInfo(@"Payout - writtent to YAP: \n\n%@\n", pay);
                        }];
                    }
                    
                    //TODO: Parse User
                    if (
                        responseDictionary[@"data"][@"profile"]
                        && [responseDictionary[@"data"][@"profile"] isKindOfClass:[NSDictionary class]]
                        )
                    {
                        NSDictionary *profileDic = [(NSDictionary *)responseDictionary[@"data"] objectForKey:@"profile"];
                        [self.currentUser parseProfileDictionary:profileDic];
                    }
                }
            }
        }

    }
                                 failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error)
    {
        //TODO: error processing
        
        ;
    }];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Referrer
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)saveReferrer:(NSString*)referrer;
{
    [self.api postReferrer:referrer
                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
    {
        
        //done: Responce analysis
        if (responseObject
            && [responseObject isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *responseDictionary = (NSDictionary *)responseObject;
            DDLogInfo(@"POST profile/referrer - checking resonse -\n\n%@",responseDictionary);
            NSNumber *status = responseDictionary[@"status"];
            
            if (status
                && [(NSNumber *)status boolValue])
            {
                //TODO: Parse USER
                if (
                    responseDictionary[@"data"]
                    && [responseDictionary[@"data"] isKindOfClass:[NSDictionary class]])
                {
                    NSDictionary *profileDic = (NSDictionary *)responseDictionary[@"data"];
                    [self.currentUser parseProfileDictionary:profileDic];
                }
            }
        }

    }
                   failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error)
    {
        ;
    }];
}

@end
