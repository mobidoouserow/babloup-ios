//
//  BUUser.m
//  BabloUp
//
//  Created by Pavel Wasilenko on 12.05.17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import "BUUser.h"

@interface BUUser () {

}

@end

@implementation BUUser

@synthesize service = service;
@synthesize token = token;
@synthesize first_name = first_name;
@synthesize last_name = last_name;
@synthesize userpic_url = userpic_url;
@synthesize email = email;
@synthesize balance = balance;
@synthesize referrer_code = referrer_code;
@synthesize referral_code = referral_code;
@synthesize referrals_amount = referrals_amount;
@synthesize referrals_earnings = referrals_earnings;
@synthesize access_token = access_token;
@synthesize video_revenue = video_revenue;
@synthesize offerwall_revenue = offerwall_revenue;

#pragma mark - initialization

- (instancetype)init;
{
    if (self = [super init])
    {
        BUUser *user = [BUUser loadUser];
        
        if (user) {
            self = user;
        }
    }
    
    return self;
}


/**
 POST http://app.babloapp.ru/api/v1/auth
 BODY:
 {
 "service": "google", /  facebook  /  vk
 "token": "01234567890abcdef"
 }
 
 RESPONCE:
 {
 "status": true,
 "data": {
 "first_name": "Вася",
 "last_name": "Пупкин",
 "userpic_url": "http://vasya.com/pupkin.png",
 "email": "vasya@pupkin.com",
 "balance": 47,
 "referrer_code": "sEB091",
 "referral_code": "Gf0tEs",
 "referrals_amount": 5,
 "referrals_earnings": 123.45,
 "access_token": "e4187c9f528099e0b6fb7493c6518a103b02ee681f209b008107ef44da8d3ec0"
 }
 }
 */

#pragma mark NSCoding

/**
 * Deserializes a Person object.
 *
 * For more information about serialization/deserialization, see the wiki article:
 * https://github.com/yaptv/YapDatabase/wiki/Storing-Objects
 **/
- (id)initWithCoder:(NSCoder *)decoder
{
    
    if (self = [super init])
    {
        service 			= [decoder decodeObjectForKey:@"service"];
        token 				= [decoder decodeObjectForKey:@"token"];
        first_name 			= [decoder decodeObjectForKey:@"first_name"];
        last_name 			= [decoder decodeObjectForKey:@"last_name"];
        userpic_url 		= [decoder decodeObjectForKey:@"userpic_url"];
        email 				= [decoder decodeObjectForKey:@"email"];
        balance 			= [decoder decodeObjectForKey:@"balance"];
        referrer_code	 	= [decoder decodeObjectForKey:@"referrer_code"];
        referral_code 		= [decoder decodeObjectForKey:@"referral_code"];
        referrals_amount 	= [decoder decodeObjectForKey:@"referrals_amount"];
        referrals_earnings 	= [decoder decodeObjectForKey:@"referrals_earnings"];
        video_revenue       = [decoder decodeObjectForKey:@"video_revenue"];
        offerwall_revenue   = [decoder decodeObjectForKey:@"offerwall_revenue"];
        access_token 		= [decoder decodeObjectForKey:@"access_token"];
    }
    return self;
}

/**
 * Serializes a Person object.
 **/
- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:service 			forKey:@"service"];
    [coder encodeObject:token 				forKey:@"token"];
    [coder encodeObject:first_name 			forKey:@"first_name"];
    [coder encodeObject:last_name 			forKey:@"last_name"];
    [coder encodeObject:userpic_url 		forKey:@"userpic_url"];
    [coder encodeObject:email 				forKey:@"email"];
    [coder encodeObject:balance 			forKey:@"balance"];
    [coder encodeObject:referrer_code	 	forKey:@"referrer_code"];
    [coder encodeObject:referral_code 		forKey:@"referral_code"];
    [coder encodeObject:referrals_amount 	forKey:@"referrals_amount"];
    [coder encodeObject:referrals_earnings 	forKey:@"referrals_earnings"];
    [coder encodeObject:video_revenue       forKey:@"video_revenue"];
    [coder encodeObject:offerwall_revenue   forKey:@"offerwall_revenue"];
    [coder encodeObject:access_token 		forKey:@"access_token"];
}

#pragma mark - saving to / loading form NSUserDefaults
- (void)save;
{
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:self];
    [[NSUserDefaults standardUserDefaults] setObject:encodedObject forKey:CURRENT_USER_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BUUser *)loadUser;
{
    DDLogInfo(@"Loading User...");
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:CURRENT_USER_KEY];
    BUUser *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
        
    return object;
}


#pragma mark - parse responce

- (BOOL)parseResponce:(id  _Nullable)responseObject
           forService:(NSString * _Nullable)inService
            withToken:(NSString * _Nullable)inToken;
{
    BOOL success = NO;
    
    if (responseObject
        && [responseObject isKindOfClass:[NSDictionary class]])
    {
        NSDictionary *responseDictionary = (NSDictionary *)responseObject;
        DDLogInfo(@"BUUser - parsing -\n\n%@",responseDictionary);
        NSNumber *status = responseDictionary[@"status"];
        
        if (status
            && [(NSNumber *)status boolValue])
        {
            //done: распарсить
            DDLogInfo(@"BUUser - status - true ");
            
            if (inToken) {
                self.token = inToken;
            }
            
            if (inService) {
                self.service = inService;
            }
            
            NSDictionary *dataDic = responseDictionary[@"data"];
            
            [self parseProfileDictionary:dataDic];
            
            if (self.access_token && self.access_token.length) {
                success = YES;
                
                [self save];
            }
        }
    }
    
    return success;
}

- (void)parseProfileDictionary:(NSDictionary *)userDic;
{
    if ([userDic[@"first_name"] isKindOfClass:[NSString class]]) {
        first_name = (NSString *)userDic[@"first_name"];
    } else {
        first_name = @"";
    }
    if ([userDic[@"last_name"] isKindOfClass:[NSString class]]) {
        last_name = (NSString *)userDic[@"last_name"];
    } else {
        last_name = @"";
    }
    if ([userDic[@"userpic_url"] isKindOfClass:[NSString class]]) {
        userpic_url = (NSString *)userDic[@"userpic_url"];
    } else {
        userpic_url = @"";
    }
    if ([userDic[@"email"] isKindOfClass:[NSString class]]) {
        email = (NSString *)userDic[@"email"];
    } else {
        email = @"";
    }
    if ([userDic[@"balance"] isKindOfClass:[NSNumber class]]) {
        balance = (NSNumber *)userDic[@"balance"];
    } else {
        balance = @0;
    }
    if ([userDic[@"referral_code"] isKindOfClass:[NSString class]]) {
        referral_code = (NSString *)userDic[@"referral_code"];
    } else {
        referral_code = @"";
    }
    if ([userDic[@"referrals_amount"] isKindOfClass:[NSNumber class]]) {
        referrals_amount = (NSNumber *)userDic[@"referrals_amount"];
    } else {
        referrals_amount = @0;
    }
    if ([userDic[@"referrals_earnings"] isKindOfClass:[NSNumber class]]) {
        referrals_earnings = (NSNumber *)userDic[@"referrals_earnings"];
    } else {
        referrals_earnings = @0;
    }
    
    if ([userDic[@"referrer_code"] isKindOfClass:[NSString class]]) {
        referrer_code = (NSString *)userDic[@"referrer_code"];
    } else {
        referrer_code = @"";
    }
    //"video_revenue": 1.5
    if ([userDic[@"video_revenue"] isKindOfClass:[NSNumber class]]) {
        video_revenue = (NSNumber *)userDic[@"video_revenue"];
    } else {
        video_revenue = @0;
    }
    if ([userDic[@"access_token"] isKindOfClass:[NSString class]]) {
        access_token = (NSString *)userDic[@"access_token"];
    } else {
        access_token = @"";
        [BUDATAMANAGER.router showInitialViewController];
    }
    if ([userDic[@"offerwall_revenue"] isKindOfClass:[NSNumber class]]) {
        offerwall_revenue = (NSNumber *)userDic[@"offerwall_revenue"];
    } else {
        offerwall_revenue = @0;
    }
    
    
    
    
//!!!
//            (lldb) po [self.referrer_code class]
//            NSNull
    
    
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:USER_UPDATED_NOTIFICATION object:nil];
}

- (NSString *)access_token {
    NSString *atk = access_token;
#ifdef DEMO
    atk = @"demo2";
#endif
    return atk;
}

- (void)clear;
{
    service = @"";
    token = @"";
    first_name = @"";
    last_name = @"";
    userpic_url = @"";
    email = @"";
    balance = @0;
    referral_code = @"";
    referrals_amount = @0;
    referrals_earnings = @0;
    referrer_code = @"";
    access_token = @"";

    [[NSNotificationCenter defaultCenter] postNotificationName:USER_UPDATED_NOTIFICATION object:nil];
    
    [self save];
}
@end
