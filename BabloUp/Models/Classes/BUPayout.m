//
//  BUPayout.m
//  BabloUp
//
//  Created by Pavel Wasilenko on 29.05.17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import "BUPayout.h"

@implementation BUPayout

@synthesize provider    = provider;
@synthesize account     = account;
@synthesize amount      = amount;
@synthesize created_at  = created_at;

@synthesize status = status;


#pragma mark - internal

- (NSString *)generatedIndex;
{
    NSString *idx = [NSString stringWithFormat:@"%@=%@=%@=%@",
                     provider,
                     account,
                     amount,
                     created_at];
    
    return idx;
}

- (NSString *)statusLocalized;
{
    NSString *loc = @"";
    
    if ([status isEqualToString:@"completed"]) {
        loc = NSLocalizedString(@"payout completed", @"status of payout - completed");
    } else if ([status isEqualToString:@"in_progress"]) {
        loc = NSLocalizedString(@"payout in progress", @"status of payout - in_progres");
    } else if ([status isEqualToString:@"rejected"]) {
        loc = NSLocalizedString(@"payout rejected", @"status of payout - rejected");
    }
    
    return loc;
}

- (NSString *)description;
{
    NSString *desc =
    [NSString stringWithFormat:@"\r"
    "generatedIndex = %@\r"
    "provider       = %@\r"
    "account        = %@\r"
    "amount         = %@\r"
    "created_at     = %@\r"
    "status         = %@\r",
     
    [self generatedIndex],
    provider,
    account,
    amount,
    created_at,
    status
     
    ];
    
    return desc;
}



#pragma mark NSCoding

/**
 * Deserializes a Person object.
 *
 * For more information about serialization/deserialization, see the wiki article:
 * https://github.com/yaptv/YapDatabase/wiki/Storing-Objects
 **/
- (id)initWithCoder:(NSCoder *)decoder
{
    
    if (self = [super init])
    {
        provider 		= [decoder decodeObjectForKey:@"provider"];
        account			= [decoder decodeObjectForKey:@"account"];
        amount          = [decoder decodeObjectForKey:@"amount"];
        created_at 		= [decoder decodeObjectForKey:@"created_at"];
        status          = [decoder decodeObjectForKey:@"status"];
    }
    return self;
}

/**
 * Serializes a Person object.
 **/
- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:provider 		forKey:@"provider"];
    [coder encodeObject:account			forKey:@"account"];
    [coder encodeObject:amount          forKey:@"amount"];
    [coder encodeObject:created_at 		forKey:@"created_at"];
    [coder encodeObject:status          forKey:@"status"];
}

#pragma mark - parsing

+ (BOOL)parseResponce:(id  _Nullable)responseObject;
{
    BOOL statusOk = NO;
    
    if (responseObject
        && [responseObject isKindOfClass:[NSDictionary class]])
    {
        NSDictionary *responseDictionary = (NSDictionary *)responseObject;
        DDLogInfo(@"BUPayout - parsing -\n\n%@",responseDictionary);
        NSNumber *status = responseDictionary[@"status"];
        
        if (status
            && [(NSNumber *)status boolValue])
        {
            //done: распарсить
            DDLogInfo(@"BUPayout - status - true ");
            
            statusOk = YES;
            if (responseDictionary[@"data"] && [responseDictionary[@"data"] isKindOfClass:[NSArray class]]) {
                DDLogInfo(@"BUPayout - data - present, is array");
                
                NSArray *dataArr = (NSArray *)responseDictionary[@"data"];
                
                NSMutableArray<BUPayout *> *pays = [NSMutableArray array];
                
                for (NSDictionary *offerDic in dataArr) {
                    
                    BUPayout *pay = [BUPayout parseFromDictionary:offerDic];
                    
                    [pays addObject:pay];
                }
                
                //done: parsed
                
                //TODO: save to Yap
                DDLogInfo(@"Payouts (%lu): \n\n%@\n", (unsigned long)pays.count, [pays firstObject]);
                
                [BUDATAMANAGER.bgConnection  asyncReadWriteWithBlock:^(YapDatabaseReadWriteTransaction * _Nonnull transaction) {
                    for (BUPayout *pay in pays) {
                        [transaction setObject:pay forKey:pay.generatedIndex inCollection:@"payout"];
                    }
                    
                    DDLogInfo(@"Payouts (%lu): writtent to YAP.", (unsigned long)pays.count);
                }];
            }
        }
    }
    
    return statusOk;
}

+ (instancetype _Nullable)parseFromDictionary:(NSDictionary * _Nullable)dic;
{
    BUPayout *pay = nil;
    
    if (dic) {
        pay = [BUPayout new];
        
        if ([dic[@"provider"] isKindOfClass:[NSString class]])
        {
            pay.provider = (NSString *)dic[@"provider"];
        }
        
        if ([dic[@"account"] isKindOfClass:[NSString class]])
        {
            pay.account = (NSString *)dic[@"account"];
        }
        
        if ([dic[@"amount"] isKindOfClass:[NSNumber class]])
        {
            pay.amount = (NSNumber*)dic[@"amount"];
        }
        
        if ([dic[@"created_at"] isKindOfClass:[NSString class]])
        {
            pay.created_at = (NSString *)dic[@"created_at"];
        }
        
        if ([dic[@"status"] isKindOfClass:[NSString class]])
        {
            pay.status = (NSString *)dic[@"status"];
        }
    }
    
    return pay;
}

+ (NSString *)stringPayoutType:(ApiClientPayoutType)type;
{
    NSString *stringType = @"";
    
    switch (type) {
        case ApiClientPayoutTypePhone:
            stringType = @"phone";
            break;
        case ApiClientPayoutTypeQiwi:
            stringType = @"qiwi";
            break;
        case ApiClientPayoutTypeWebmoney:
            stringType = @"webmoney";
            break;
        default:
            break;
    }
    
    return stringType;
}

@end
