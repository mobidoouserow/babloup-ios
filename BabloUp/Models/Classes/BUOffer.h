//
//  BUOffer.h
//  BabloUp
//
//  Created by Pavel Wasilenko on 27.05.17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BUOffer : NSObject <NSCoding>

@property (nonatomic, strong, nonnull) NSNumber *offer_id;
@property (nonatomic, strong, nonnull, readonly) NSString *offer_id_str;
@property (nonatomic, strong, nullable) NSString *title;
@property (nonatomic, strong, nullable) NSString *logo;
@property (nonatomic, strong, nullable) NSString *link;
@property (nonatomic, strong, nullable) NSString *created_at;
//active/completed/null
@property (nonatomic, strong, nullable) NSString *status;
@property (nonatomic, assign) ApiClientOfferStatus statusEnum;

//install/task/null
@property (nonatomic, strong, nullable) NSString *task_type;
@property (nonatomic, strong, nullable) NSArray<NSString *> *task_description;
@property (nonatomic, strong, nullable) NSString *package;
@property (nonatomic, strong, nullable) NSNumber *revenue;


+ (NSString * _Nullable)offerStatusFromEnum:(ApiClientOfferStatus)statysEnum;
+ (ApiClientOfferStatus)offerStatusFromString:(NSString * _Nullable)statysEnum;

+ (BOOL)parseResponce:(id  _Nullable)responseObject;
+ (instancetype _Nullable)parseFromDictionary:(NSDictionary * _Nullable)dic;

@end
