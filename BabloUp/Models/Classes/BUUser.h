//
//  BUUser.h
//  BabloUp
//
//  Created by Pavel Wasilenko on 12.05.17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BUUser : NSObject <NSCoding>

@property (nonatomic, strong, nullable) NSString *service;
@property (nonatomic, strong, nullable) NSString *token;

@property (nonatomic, strong, nullable) NSString *first_name;
@property (nonatomic, strong, nullable) NSString *last_name;

@property (nonatomic, strong, nullable) NSString *userpic_url;
@property (nonatomic, strong, nullable) NSString *email;
@property (nonatomic, strong, nullable) NSNumber *balance;
@property (nonatomic, strong, nullable) NSString *referrer_code;
@property (nonatomic, strong, nullable) NSString *referral_code;
@property (nonatomic, strong, nullable) NSNumber *referrals_amount;
@property (nonatomic, strong, nullable) NSNumber *referrals_earnings;
@property (nonatomic, strong, nullable) NSString *access_token;
@property (nonatomic, strong, nullable) NSNumber *video_revenue;
@property (nonatomic, strong, nullable) NSNumber *offerwall_revenue;

///Saves user to NSUserDefaults
- (void)save;

///Saves user to NSUserDefaults
+ (BUUser * _Nullable)loadUser;

///Login method - stores service and token and 
- (BOOL)parseResponce:(id  _Nullable)responseObject
           forService:(NSString * _Nullable)service
            withToken:(NSString * _Nullable)token;

///Parse user (profile) dictionary and send notification
- (void)parseProfileDictionary:(NSDictionary * _Nullable)userDic;

///Clear the user content and send notification
- (void)clear;

@end
