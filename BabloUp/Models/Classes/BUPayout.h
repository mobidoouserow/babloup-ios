//
//  BUPayout.h
//  BabloUp
//
//  Created by Pavel Wasilenko on 29.05.17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BUPayout : NSObject

/*
 "provider": "webmoney",
 "account": "1234567887654321",
 "amount": 500,
 "created_at": "2017-05-02 14:54:01",
 "status": "completed"
 */

@property (nonatomic, strong, nullable) NSString *provider;
@property (nonatomic, strong, nullable) NSString *account;
@property (nonatomic, strong, nullable) NSNumber *amount;
@property (nonatomic, strong, nullable) NSString *created_at;
//in_progress/completed/rejected
@property (nonatomic, strong, nullable) NSString *status;
@property (nonatomic, strong, nullable) NSString *statusLocalized;

@property (nonatomic, strong, nullable, readonly) NSString *generatedIndex;


+ (BOOL)parseResponce:(id  _Nullable)responseObject;
+ (instancetype _Nullable)parseFromDictionary:(NSDictionary * _Nullable)dic;

+ (NSString *_Nonnull)stringPayoutType:(ApiClientPayoutType)type;

@end
