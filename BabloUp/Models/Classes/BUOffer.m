//
//  BUOffer.m
//  BabloUp
//
//  Created by Pavel Wasilenko on 27.05.17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import "BUOffer.h"

@implementation BUOffer

@synthesize offer_id = offer_id;
@synthesize offer_id_str = _offer_id_str;
@synthesize title = title;
@synthesize logo = logo;
@synthesize link = link;
@synthesize created_at = created_at;
@synthesize status = status;
@synthesize task_type = task_type;
@synthesize task_description = task_description;
@synthesize package = package;
@synthesize revenue = revenue;


/**
 
 status
 boolean
 Cтатус ответа.
 true
 
 data
 array
 0
 object
 id
 number
 Идентификатор оффера.
 1
 title
 string
 Название оффера.
 Vikings: War of Clans [iPad] RU (Mytarget + FB cloaking)
 logo
 string
 URL иконки оффера.
 http://offers.zorkanetwork.com/images/cpa/logos/3488930199.png
 link
 string
 Ссылка на выполнение задания оффера.
 http://track.zorkanetwork.com/click?pid=1731&offer_id=134
 created_at
 string
 Дата появления оффера.
 2017-04-11 16:13:11
 status
 enum
 Статус оффера для текущего пользователя.
 active
 string
 Активный.
 completed
 string
 Выполненный.
 SAMPLE
 DEFAULT
 null
 string
 task_type
 enum
 Тип задания оффера.
 install
 string
 Скачать и установить.
 task
 string
 Выполнить большое задание.
 SAMPLE
 install
 string
 DEFAULT
 null
 string
 task_description
 array
 Описание задания оффера.
 0
 Установить игру
 string
 1
 Пройти тренировку
 string
 2
 Построить 2 крепости
 string
 package
 string
 Пакет или bundle ID приложения. Если не null - клиент должен проверять самостоятельно факт установки приложения на устройство.
 com.bookmaker
 DEFAULT
 null
 revenue
 number
 Вознаграждение за прохождение задания.
 15
 
 */
 
 #pragma mark NSCoding
 
 /**
 * Deserializes a BUOffer object.
 *
 * For more information about serialization/deserialization, see the wiki article:
 * https://github.com/yaptv/YapDatabase/wiki/Storing-Objects
 **/
- (id)initWithCoder:(NSCoder *)decoder
{
    if ((self = [super init]))
    {
        offer_id   	= [decoder decodeObjectForKey:@"offer_id"];
        _offer_id_str = [NSString stringWithFormat:@"%@", offer_id];
        title       = [decoder decodeObjectForKey:@"title"];
        logo        = [decoder decodeObjectForKey:@"logo"];
        link        = [decoder decodeObjectForKey:@"link"];
        created_at  = [decoder decodeObjectForKey:@"created_at"];
        status      = [decoder decodeObjectForKey:@"status"];
        _statusEnum  = [BUOffer offerStatusFromString:status];
        task_type   = [decoder decodeObjectForKey:@"task_type"];
        task_description = [decoder decodeObjectForKey:@"task_description"];
        package     = [decoder decodeObjectForKey:@"package"];
        revenue     = [decoder decodeObjectForKey:@"revenue"];
    }
    return self;
}

/**
 * Serializes a BUOffer object.
 **/
- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:offer_id    forKey:@"offer_id"];
    [coder encodeObject:title       forKey:@"title"];
    [coder encodeObject:logo        forKey:@"logo"];
    [coder encodeObject:link        forKey:@"link"];
    [coder encodeObject:created_at  forKey:@"created_at"];
    [coder encodeObject:status      forKey:@"status"];
    [coder encodeObject:task_type   forKey:@"task_type"];
    [coder encodeObject:task_description forKey:@"task_description"];
    [coder encodeObject:package     forKey:@"package"];
    [coder encodeObject:revenue     forKey:@"revenue"];
}


#pragma mark - status enum <-> string

+ (NSString * _Nullable)offerStatusFromEnum:(ApiClientOfferStatus)statusEnum;
{
    NSString *statusString = @"";
    
    switch (statusEnum) {
        case ApiClientOfferStatusCompleted:
            statusString = @"completed";
            break;
        case ApiClientOfferStatusActive:
            statusString = @"active";
            break;
        case ApiClientOfferStatusNew:
            statusString = @"new";
            break;
        default:
            break;
    }
    
    return statusString;
}

+ (ApiClientOfferStatus)offerStatusFromString:(NSString * _Nullable)statusString;
{
    ApiClientOfferStatus statusEnum = ApiClientOfferStatusNew;
    
    if (statusString && statusString.length > 0) {
        if ([statusString isEqualToString:@"active"]) {
            statusEnum = ApiClientOfferStatusActive;
        } else if ([statusString isEqualToString:@"completed"]) {
            statusEnum = ApiClientOfferStatusCompleted;
        } else if ([statusString isEqualToString:@"new"]) {
            statusEnum = ApiClientOfferStatusNew;
        }
    }
    
    return statusEnum;
}

#pragma mark - Description

//TODO: create description, debugDescription
/*
- (NSString *)debugDescription {
    
    NSString * desc = @""
    ""
    ;
    
    
    return desc;
}
*/
- (NSString *)description {
    
    NSMutableString * desc = [@"" mutableCopy];
    [desc appendString:@"\r"];
    [desc appendFormat:@"offer_id         = %@\r", offer_id];
    [desc appendFormat:@"_offer_id_str    = %@\r", _offer_id_str];
    [desc appendFormat:@"title            = %@\r", title];
    [desc appendFormat:@"logo             = %@\r", logo];
    [desc appendFormat:@"link             = %@\r", link];
    [desc appendFormat:@"created_at       = %@\r", created_at];
    [desc appendFormat:@"status           = %@\r", status];
    [desc appendFormat:@"_statusEnum      = %@\r", [BUOffer offerStatusFromEnum:_statusEnum]];
    [desc appendFormat:@"task_type        = %@\r", task_type];
    [desc appendFormat:@"task_description = %@\r", task_description];
    [desc appendFormat:@"package          = %@\r", package];
    [desc appendFormat:@"revenue          = %@\r", revenue];
    
    return [desc copy];
}

#pragma mark - setters - offer_id<->offer_id_str + status<->statusEnum

- (NSString *)offer_id_str;
{
    NSString *str = [NSString stringWithFormat:@"%@", offer_id];
    return str;
}

- (void)setOffer_id:(NSNumber *)inOffer_id;
{
    offer_id = inOffer_id;
    _offer_id_str = [NSString stringWithFormat:@"%@", offer_id];
}

- (void)setStatus:(NSString *)inStatus;
{
    status = inStatus;
    _statusEnum = [BUOffer offerStatusFromString:inStatus];
}

- (void)setStatusEnum:(ApiClientOfferStatus)inStatusEnum;
{
    _statusEnum = inStatusEnum;
    status = [BUOffer offerStatusFromEnum:inStatusEnum];
}

#pragma mark - parsing JSON

/**
{
    "status": true,
    "data": [
             {
                 "id": 1,
                 "title": "Vikings: War of Clans [iPad] RU (Mytarget + FB cloaking)",
                 "logo": "http://offers.zorkanetwork.com/images/cpa/logos/3488930199.png",
                 "link": "http://track.zorkanetwork.com/click?pid=1731&offer_id=134",
                 "created_at": "2017-04-11 16:13:11",
                 "status": "active",
                 "task_type": "install",
                 "task_description": [
                                      "Установить игру",
                                      "Пройти тренировку",
                                      "Построить 2 крепости"
                                      ],
                 "package": "com.bookmaker",
                 "revenue": 15
             }
             ],
    "meta": {
        "page": 1,
        "per_page": 500,
        "total_elements": 345512
    }
}
*/

+ (BOOL)parseResponce:(id  _Nullable)responseObject;
{
    BOOL statusOk = NO;
    
    if (responseObject
        && [responseObject isKindOfClass:[NSDictionary class]])
    {
        NSDictionary *responseDictionary = (NSDictionary *)responseObject;
        DDLogInfo(@"BUOffer - parsing - allKeys\n\n%@",responseDictionary.allKeys);
        NSNumber *status = responseDictionary[@"status"];
        
        if (status
            && [(NSNumber *)status boolValue])
        {
            //done: распарсить
            DDLogInfo(@"BUOffer - status - true ");
            
            statusOk = YES;
            if (responseDictionary[@"data"] && [responseDictionary[@"data"] isKindOfClass:[NSArray class]]) {
                DDLogInfo(@"BUOffer - data - present, is array");
                
                NSArray *dataArr = (NSArray *)responseDictionary[@"data"];
                
                NSMutableArray<BUOffer *> *offers = [NSMutableArray array];
                
                for (NSDictionary *offerDic in dataArr) {
                    
                    BUOffer *offer = [BUOffer parseFromDictionary:offerDic];
                    
                    [offers addObject:offer];
                }
                
                //done: parsed
                
                //TODO: save to Yap
                DDLogInfo(@"Offers (%lu): \n\n%@\n", (unsigned long)offers.count, [offers firstObject]);
                
                [BUDATAMANAGER.bgConnection asyncReadWriteWithBlock:^(YapDatabaseReadWriteTransaction * _Nonnull transaction) {
                    for (BUOffer *off in offers) {
                        
                        [transaction setObject:off forKey:off.offer_id_str inCollection:@"offer"];
                    }
                    
                    DDLogInfo(@"Offers (%lu): writtent to YAP.", (unsigned long)offers.count);
                }];
            }
        }
    }
    
    return statusOk;
}

+ (instancetype)parseFromDictionary:(NSDictionary *)dic;
{
    
    BUOffer *offer = nil;
    
    if (dic) {
        offer = [BUOffer new];
        
        if ([dic[@"id"] isKindOfClass:[NSNumber class]]) {
            offer.offer_id = (NSNumber *)dic[@"id"];
        } else {
            offer.offer_id = @0;
        }
        //assigns automatically
//        offer.offer_id_str = [NSString stringWithFormat:@"%@", offer.offer_id];
        
        if ([dic[@"title"] isKindOfClass:[NSString class]]) {
            offer.title = (NSString *)dic[@"title"];
        }
        else
        {
            DDLogWarn(@"offer %@ - no title !", offer.offer_id);
        }
        
        if ([dic[@"logo"] isKindOfClass:[NSString class]]) {
            offer.logo = (NSString *)dic[@"logo"];
        }
        if ([dic[@"link"] isKindOfClass:[NSString class]]) {
            offer.link = (NSString *)dic[@"link"];
        } else {
            offer.link = @"";
        }
        if ([dic[@"created_at"] isKindOfClass:[NSString class]]) {
            offer.created_at        = (NSString *)dic[@"created_at"];
        } else {
            offer.created_at = @"";
        }
        //may contain <null>
        
        if ([dic[@"status"] isKindOfClass:[NSString class]]) {
            offer.status = (NSString *)dic[@"status"];
        } else {
            offer.status = @"new";
        }
        
        offer.statusEnum = [BUOffer offerStatusFromString:offer.status];
        
        //??? it's an array of strings
        NSMutableArray *desc = [NSMutableArray array];
        
        if ([dic[@"task_description"] isKindOfClass:[NSArray class]]) {
            for (NSString *td in dic[@"task_description"]) {
                [desc addObject:(NSString *)td];
            }
        }
        
        offer.task_description  = desc;
        
        // may contain <null>
        if ([dic[@"package"] isKindOfClass:[NSString class]]) {
            offer.package           = dic[@"package"];
        } else {
            offer.package = @"";
        }
        if ([dic[@"task_type"] isKindOfClass:[NSString class]]) {
            offer.task_type = (NSString *)dic[@"task_type"];
        } else  {
            offer.task_type = @"";
        }
        if ([dic[@"revenue"] isKindOfClass:[NSNumber class]]) {
            offer.revenue = (NSNumber *)dic[@"revenue"];
        } else {
            offer.revenue = @0;
        }
    }
    
    return offer;
}

@end
