//
//  BUProfilePartVC.m
//  BabloUp
//
//  Created by Pavel Wasilenko on 05/04/17.
//  Copyright © 2017 Bars-i-Lis. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "BUProfilePartVC.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "BUBaseView.h"

@interface BUProfilePartVC ()
@property (weak, nonatomic) IBOutlet UIButton *emailButton;
@property (weak, nonatomic) IBOutlet UILabel *coinsCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *referralsCountLabel;
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;

@property (weak, nonatomic) IBOutlet BUBaseView *photoContainerView;

@end

@implementation BUProfilePartVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [self refreshUserData];
    
    [BUDATAMANAGER updateUser];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshUserData)
                                                 name:USER_UPDATED_NOTIFICATION
                                               object:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)refreshUserData;
{
    if (BUDATAMANAGER.currentUser.email) {
        [_emailButton setTitle:BUDATAMANAGER.currentUser.email forState:UIControlStateNormal];
        [_emailButton setTitle:BUDATAMANAGER.currentUser.email forState:UIControlStateHighlighted];
        [_emailButton setTitle:BUDATAMANAGER.currentUser.email forState:UIControlStateDisabled];
        [_emailButton setTitle:BUDATAMANAGER.currentUser.email forState:UIControlStateSelected];
    }
    
    if (BUDATAMANAGER.currentUser.referrals_amount) {
        self.referralsCountLabel.text = [NSString stringWithFormat:@"%@", BUDATAMANAGER.currentUser.referrals_amount];
    }
        
    if (BUDATAMANAGER.currentUser.balance) {
        self.coinsCountLabel.text = [NSString stringWithFormat:@"%@", BUDATAMANAGER.currentUser.balance];
    }
    
    if (BUDATAMANAGER.currentUser.userpic_url) {
        
        NSURL *url = [NSURL URLWithString:BUDATAMANAGER.currentUser.userpic_url];
        
        if (url) {
            [self.photoImageView setImageWithURL:url
                                placeholderImage:[UIImage imageNamed:@"Menu-Account"]];
        }
    }
}


#pragma mark - actions

- (IBAction)emailButtonClick:(id)sender
{
    
}

@end
