//
//  BURouterController.m
//  BabloUp
//
//  Created by Pavel Wasilenko on 08/04/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import "BURouterController.h"

#import "BUSignUpVC.h"

#import "BUMenuVC.h"

#import "BUOffersVC.h"
#import "BUAccountVC.h"
#import "BUCoinsVC.h"
#import "BUPartnersVC.h"

@interface BURouterController ()

@property (nonatomic, strong) UIWindow *window;

@property (strong, nonatomic) BUSignUpVC *signUpVC;
//@property (strong, nonatomic) UINavigationController *signUpVCNav;

@property (strong, nonatomic) BUMenuVC *menuVC;


@property (strong, nonatomic) BUOffersVC *offersVC;
//@property (strong, nonatomic) UINavigationController *offersVCNav;

@property (strong, nonatomic) BUAccountVC *accountVC;
//@property (strong, nonatomic) UINavigationController *accountVCNav;

@property (strong, nonatomic) BUCoinsVC *coinsVC;
//@property (strong, nonatomic) UINavigationController *coinsVCNav;

@property (strong, nonatomic) BUPartnersVC *partnersVC;
//@property (strong, nonatomic) UINavigationController *partnersVCNav;


//@property (strong, nonatomic) BUSingleOfferVC *singleOfferVC;


@end

@implementation BURouterController

#pragma mark - init

- (instancetype)init {
    if (self = [super init]) {
        [self initAll];
    }
    
    return self;
}


- (void)initAll {
    if (!_window) {
        _window = UIApplication.sharedApplication.delegate.window;
    }
    
    if (!_signUpVC) {
        _signUpVC = [BUSignUpVC storyboardInstance];
//        _signUpVCNav = [self embedInNavifgationController:_signUpVC];
    }
    
    if (!_offersVC) {
        _offersVC = [BUOffersVC storyboardInstance];
//        _offersVCNav = [self embedInNavifgationController:_offersVC];
    }
    
    if (!_accountVC) {
        _accountVC = [BUAccountVC storyboardInstance];
//        _accountVCNav = [self embedInNavifgationController:_accountVC];
    }
    
    if (!_coinsVC) {
        _coinsVC = [BUCoinsVC storyboardInstance];
    }
    
    if (!_partnersVC) {
        _partnersVC = [BUPartnersVC storyboardInstance];
    }
    
    
    if (!_menuVC) {
        _menuVC = [BUMenuVC storyboardInstance];
    }
    
    if(!_navigationController) {
        _navigationController = [[UINavigationController alloc] init];
        
        [_navigationController setToolbarHidden:YES];
    }
    
    //init sideMenu
    _sideMenuController =
    [LGSideMenuController sideMenuControllerWithRootViewController:_navigationController
                                                leftViewController:_menuVC
                                               rightViewController:nil];
    
    //done: detect 320pt iphone. make width 320 * 330/375 = 282
    //TODO: ??? detect iPhone Plus controller, make width 414 * 330/375 =
    _sideMenuController.leftViewWidth = [BUUtility isSmallerThenIPhone6] ? 282.0f : 330.0f;
    _sideMenuController.leftViewPresentationStyle = LGSideMenuPresentationStyleSlideBelow;
}

-(UINavigationController*)embedInNavifgationController:(UIViewController *)vc {
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:vc];
    
    [navigation setNavigationBarHidden:YES animated:NO];
    
    return navigation;
}

-(void)initWindowAndNav;
{
    if (!_window) {
        _window = UIApplication.sharedApplication.delegate.window;
    }
    if (!_navigationController)
    {
        _navigationController = [[UINavigationController alloc] init];
        [_navigationController setToolbarHidden:YES];
    }
}

#pragma mark - Menu

- (void)menuToggle;
{
    if (!_sideMenuController.isLeftViewVisible) {
        [_sideMenuController showLeftViewAnimated];
    } else {
        [_sideMenuController hideLeftViewAnimated];
    }
}

- (void)showMenu;
{
    if (!_sideMenuController.isLeftViewVisible) {
        [_sideMenuController showLeftViewAnimated];
    }
}

- (void)hideMenu;
{
    if (_sideMenuController.isLeftViewVisible) {
        [_sideMenuController hideLeftViewAnimated];
    }
}

#pragma mark - currentVC

- (BOOL)isLoginShowing {
    BOOL isLogin = NO;
    if (_navigationController
        && _navigationController.viewControllers.count
        && [_navigationController.topViewController isKindOfClass:[BUSignUpVC class]]
        )
    {
        isLogin = YES;
    }
    
    return isLogin;
}

#pragma mark - Navigation

- (void)showInitialViewController;
{
    [self initWindowAndNav];
    
//    if ([[BUDataManager instance] isTokenStored]) {
        //Authorized - showing menu
        [self showOffers];
    
    if (![BUDATAMANAGER isTokenStored]) {
        NSString *device_id = [UIDevice currentDevice].identifierForVendor.UUIDString;
        if (device_id.length) {
            [BUDATAMANAGER authorizeUserWithService:nil socialToken:nil];
        } else {
            [[BUDataManager instance] performSelector:@selector(authorizeUserWithService:socialToken:) withObject:nil afterDelay:1.0f];
        }
    } else {
        [BUDATAMANAGER updateUser];
    }
//    } else {
//        //Not Authorized - showing signUp
//        
//        [self showSignUp];
//    }
}


- (void)showSignUp;
{
    [self initWindowAndNav];
    
    if (!_navigationController.viewControllers.lastObject
        || ![_navigationController.viewControllers.lastObject isKindOfClass:[BUSignUpVC class]])
    {
        [_navigationController setViewControllers:@[_signUpVC] animated:YES];
    }
    
    _sideMenuController.leftViewSwipeGestureEnabled = NO;
    _sideMenuController.rightViewSwipeGestureEnabled = NO;
}

- (void)showOffers {
    [self initWindowAndNav];
    
    if (!_navigationController.viewControllers.firstObject
        || ![_navigationController.viewControllers.firstObject isKindOfClass:[BUOffersVC class]])
    {
        [_navigationController setViewControllers:@[_offersVC] animated:YES];
    }
    
    [self.sideMenuController hideLeftViewAnimated];
}

- (void)showAccount;
{
    [self initWindowAndNav];
    
    if (!_navigationController.viewControllers.firstObject
        || ![_navigationController.viewControllers.firstObject isKindOfClass:[BUAccountVC class]])
    {
        [_navigationController setViewControllers:@[_accountVC] animated:YES];
    }
    
    [self.sideMenuController hideLeftViewAnimated];
}

- (void)showCoins;
{
    [self initWindowAndNav];
    
    if (!_navigationController.viewControllers.firstObject
        || ![_navigationController.viewControllers.firstObject isKindOfClass:[BUCoinsVC class]])
    {
        [_navigationController setViewControllers:@[_coinsVC] animated:YES];
    }
    
    [self.sideMenuController hideLeftViewAnimated];
}

- (void)showPartners;
{
    [self initWindowAndNav];
    
    if (!_navigationController.viewControllers.firstObject
        || ![_navigationController.viewControllers.firstObject isKindOfClass:[BUPartnersVC class]])
    {
        [_navigationController setViewControllers:@[_partnersVC] animated:YES];
    }
    
    [self.sideMenuController hideLeftViewAnimated];
}


#pragma mark - VKSdkUIDelegate

//-(void)vkSdkShouldPresentViewController:(UIViewController *)controller {
//    //done: 1. determine current top view controller
//    //done: 2. show controller
//    
//    [self.navigationController presentViewController:controller animated:YES completion:nil];
//}
//
//-(void)vkSdkNeedCaptchaEnter:(VKError *)captchaError {
//    //done: 1. determine current top view controller
//    //done: 2. show capcha
//    
//    VKCaptchaViewController *captchaVC = [VKCaptchaViewController captchaControllerWithError:captchaError];
//    [self.navigationController presentViewController:captchaVC animated:YES completion:nil];
//}


@end
