//
//  BUCoinsVCHeaderCell.h
//  BabloUp
//
//  Created by John FrostFox on 13/05/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BUCoinsVCHeaderCell : UITableViewCell

@property (nonatomic, weak) UITableView *tableView;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, strong) NSObject *model;

@property (nonatomic, strong) UIViewController *parentVC;

@end
