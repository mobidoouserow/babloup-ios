//
//  BUCoinsVCPayoutCell.m
//  BabloUp
//
//  Created by John FrostFox on 13/05/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import "BUCoinsVCPayoutCell.h"

#import "BUPayout.h"

@interface BUCoinsVCPayoutCell ()
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UILabel *accountLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@end

@implementation BUCoinsVCPayoutCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self prepareForReuse];
}

- (void)prepareForReuse;
{
    [super prepareForReuse];
    //done: nulify labels
    self.amountLabel.text   = @"";
    self.accountLabel.text  = @"";
    self.dateLabel.text     = @"";
    self.statusLabel.text   = @"";
}

- (void)configure;
{
//    provider, ???
    
    if (self.model && [self.model isKindOfClass:[BUPayout class]]) {
        BUPayout *pay = (BUPayout *)self.model;
        
        //TODO: localize
        self.amountLabel.text   = [NSString stringWithFormat:@"%@ руб", pay.amount];
        self.accountLabel.text  = pay.account;
        self.dateLabel.text     = pay.created_at;
        
        //TODO: localize
        self.statusLabel.text   = pay.statusLocalized;
    }

}

@end
