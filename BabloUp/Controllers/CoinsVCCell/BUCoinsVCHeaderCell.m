//
//  BUCoinsVCHeaderCell.m
//  BabloUp
//
//  Created by John FrostFox on 13/05/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import "BUCoinsVCHeaderCell.h"

#import "UIView+BU.h"
#import "BUUser.h"

typedef enum : NSUInteger {
    BUPayoutTypePhone,
    BUPayoutTypeWM,
    BUPayoutTypeQiwi,
} BUPayoutType;

@interface BUCoinsVCHeaderCell () {
    ApiClientPayoutType _payoutProvider;
}




@property (weak, nonatomic) IBOutlet UILabel *bigCoinsAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *coinsAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *rubAmountLabel;

@property (weak, nonatomic) IBOutlet UITextField *accountTextField;
@property (weak, nonatomic) IBOutlet UITextField *amountTextField;

@property (weak, nonatomic) IBOutlet UIButton *phoneButton;
@property (weak, nonatomic) IBOutlet UIButton *wmButton;
@property (weak, nonatomic) IBOutlet UIButton *qiwiButton;

@end

@implementation BUCoinsVCHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
    _phoneButton.selected = YES;
    _phoneButton.backgroundColor = [UIColor buttonSelectedBackgroundColor];
    _phoneButton.borderUIColor = [UIColor buttonSelectedBorderColor];
    _phoneButton.borderWidth = 1.0;
    
    _payoutProvider = ApiClientPayoutTypePhone;
    
    [_phoneButton setNeedsLayout];
    
    _wmButton.backgroundColor = [UIColor buttonBackgrounddColor];
    
    _qiwiButton.backgroundColor = [UIColor buttonBackgrounddColor];
    
    [self renewUserData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(renewUserData) name:USER_UPDATED_NOTIFICATION object:nil];
}

- (void)renewUserData;
{
    NSInteger coins = BUDATAMANAGER.currentUser.balance.integerValue;
    
    self.bigCoinsAmountLabel.text = [NSString stringWithFormat:@"%ld", (long)coins];
    self.coinsAmountLabel.text = [NSString stringWithFormat:@"%ld монет", (long)coins];
    self.rubAmountLabel.text = [NSString stringWithFormat:@"%.02f рублей", (float)coins / 10.0f];
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)createPaymentButtonClick:(id)sender {
    //done: считать и проверить значения из текстовых полей
    
    NSString *sumString = self.amountTextField.text;
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    
    NSNumber *sum = [f numberFromString:sumString];
    
    if (sum == nil) {
        [BUDATAMANAGER showAlertErrorWithMessage:@"Введите Сумму вывода!"];
        
        return;
    }
    
    NSString *account = [self.accountTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (!account || account.length == 0) {
        //TODO: вынести в отдельный метод. ? DataManager ?
        [BUDATAMANAGER showAlertErrorWithMessage:@"Введите Телефон/Счёт!"];
         
        return;
    } else {
        /*
         d.pugachev
         [12:15 PM]
         Паттерн preg
         webmoney: `^[rR][\d]{12}$`
         qiwi: `^\d{11}$`
         phone: `^\d{11}$`
         */
        BOOL matches = NO;
        
        NSString *regexString =  @"^\\d{11}$";
        
        if (_payoutProvider == ApiClientPayoutTypeWebmoney) {
            regexString = @"^[rR][\\d]{12}$";
        }
        
        NSError *regexError = NULL;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexString
                                                                               options:NSRegularExpressionCaseInsensitive
                                                                                 error:&regexError];
        
        NSRange rangeOfFirstMatch = [regex rangeOfFirstMatchInString:account options:0 range:NSMakeRange(0, [account length])];

        if(!NSEqualRanges(rangeOfFirstMatch, NSMakeRange(NSNotFound, 0))
           && NSEqualRanges(rangeOfFirstMatch, NSMakeRange(0, account.length))
           )
        {
            DDLogInfo(@"Matched!\n");
            matches = YES;
        } else {
            [BUDATAMANAGER showAlertErrorWithMessage:@"Телефон/Счёт в неверном формате!"];
            
            return;
        }

        
    }
    
    [BUDATAMANAGER createPaymentOfType:_payoutProvider
                                amount:sum
                               account:account];
}

- (IBAction)phoneButtonClick:(id)sender {
    if(!_phoneButton.selected) {
        [self select:YES button:_phoneButton];
        [self select:NO button:_wmButton];
        [self select:NO button:_qiwiButton];
        _payoutProvider = ApiClientPayoutTypePhone;
        self.accountTextField.placeholder = @"79871112233";
    }
}

- (IBAction)wmButtonClick:(id)sender {
    if (!_wmButton.selected) {
        [self select:YES button:_wmButton];
        [self select:NO button:_qiwiButton];
        [self select:NO button:_phoneButton];
        _payoutProvider = ApiClientPayoutTypeWebmoney;
        self.accountTextField.placeholder = @"R012345678901";
    }
}

- (IBAction)qiwiButtonClick:(id)sender {
    if(!_qiwiButton.selected) {
        [self select:YES button:_qiwiButton];
        [self select:NO button:_wmButton];
        [self select:NO button:_phoneButton];
        _payoutProvider = ApiClientPayoutTypeQiwi;
        self.accountTextField.placeholder = @"79871112233";
    }
}

- (void)select:(BOOL)select button:(UIButton *)button;
{
    if (button.selected == select) {
        return;
    }
    
    button.selected = select;
    
    //done: Add animation
    
    if (select) {
        [UIView animateWithDuration:0.3 animations:^{
            button.borderWidth = 1.0f;
            button.backgroundColor = [UIColor buttonSelectedBackgroundColor];
        }];
    } else {
        [UIView animateWithDuration:0.3 animations:^{
            button.borderWidth = 0.0f;
            button.backgroundColor = [UIColor buttonBackgrounddColor];
        }];
    }
    
    [button setNeedsLayout];
}

@end
