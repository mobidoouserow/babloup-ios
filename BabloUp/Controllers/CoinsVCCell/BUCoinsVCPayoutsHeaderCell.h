//
//  BUCoinsVCPayoutsHeaderCell.h
//  BabloUp
//
//  Created by John FrostFox on 13/05/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BUCoinsVCPayoutsHeaderCell : UIView
@property (weak, nonatomic) IBOutlet UIView *contentView;

@end
