//
//  BUSignUpVC.m
//  BabloUp
//
//  Created by Pavel Wasilenko on 04/04/17.
//  Copyright © 2017 Bars-i-Lis. All rights reserved.
//

#import "BUSignUpVC.h"
//#import <VKSdk.h>

@interface BUSignUpVC ()

@end

@implementation BUSignUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Click

- (IBAction)googleButtonClick:(id)sender {
    ;
}

- (IBAction)vkontakteButtonClick:(id)sender {

//    //TODO: put this completly in BUDataManager *
//    [VKSdk wakeUpSession:VK_APP_SCOPE completeBlock:^(VKAuthorizationState state, NSError *error) {
//        switch (state) {
//            case VKAuthorizationAuthorized:
//                // User already autorized, and session is correct
//                
//                //TODO: API - send token
//                [BUDATAMANAGER authorizeUserWithService:@"vk"
//                                            socialToken:[VKSdk accessToken].accessToken];
//                
//                
//                break;
//                
//            case VKAuthorizationInitialized:
//                // User not yet authorized, proceed to next step
//                
//                //done: VK Auth
//                [VKSdk authorize:VK_APP_SCOPE withOptions:VKAuthorizationOptionsUnlimitedToken];
//                
//                break;
//                
//            default:
//                // Probably, network error occured, try call +[VKSdk wakeUpSession:completeBlock:] later
//                break;
//        }
//    }];
}

- (IBAction)facebookButtonClick:(id)sender {
    
}

- (IBAction)termsAndPrivacyClick:(id)sender {
    //TODO: хардкодный залогин - убрать перед реализацией.
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Token"
                                                                              message: @"Input user token!"
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"token";
//        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
//        textField.borderStyle = UITextBorderStyleRoundedRect;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * tokenTextField = textfields[0];
        
        NSString *token = [tokenTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (token && token.length) {
            BUDATAMANAGER.currentUser.access_token = token;
            [BUDATAMANAGER createAndSetupDatabase];
            
            [BUDATAMANAGER updateUser];
            
            [BUDATAMANAGER.router showInitialViewController];
        }
    }]];
    [BUDATAMANAGER.router.navigationController.topViewController presentViewController:alertController animated:YES completion:nil];
    
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://babloapp.ru/"]];
    
    //TODO: узнать URL "Terms of Service and Privacy Policy"
}

@end
