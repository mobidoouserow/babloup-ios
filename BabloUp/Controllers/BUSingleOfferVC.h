//
//  BUSingleOfferVC.h
//  BabloUp
//
//  Created by Paul V. @bis_nival @telegram on 6/1/17
//  Copyright (c) 2017 Bars-i-Lis. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BUOffer;

@interface BUSingleOfferVC : BUBaseViewController

@property (nonatomic, strong) BUOffer *offer;

- (void)configure;

@end
