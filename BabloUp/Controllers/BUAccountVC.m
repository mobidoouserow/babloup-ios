//
//  BUAccountVC.m
//  BabloUp
//
//  Created by Paul V. @bis_nival @telegram on 3/24/17
//  Copyright (c) 2014 Bars-i-Lis. All rights reserved.
//

#import "BUAccountVC.h"
#import "BUProfilePartVC.h"

@interface BUAccountVC ()<UITableViewDelegate, UITableViewDataSource> {
    NSArray *_title;
    NSArray<NSNumber *> *_enabled;
    NSArray *_image;
}

@property (weak, nonatomic) IBOutlet UIView *navBarBGVew;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *profilePartPlaceholderView;

@property (strong, nonatomic) BUProfilePartVC *profilePartVC;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *menuBGViewWidthConstraint;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end


@implementation BUAccountVC

- (IBAction)menuButtonClick:(id)sender {
    [[BUDataManager instance].router menuToggle];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.hidden = YES;
    
    self.titleLabel.hidden = YES;
    
    //array of puncts titles
    _title =
    @[
      @"Отключено",
      @"Отключено",
      @"Отключено",
      @"Выйти из аккаунта",
      ];
    
    //array of puncts images
    _image =
    @[
      [UIImage imageNamed:@"SignUp-G"],
      [UIImage imageNamed:@"SignUp-FB"],
      [UIImage imageNamed:@"SignUp-VK"],
      [UIImage imageNamed:@"Account-Logout"],
      ];
    
    _enabled =
    @[
      @NO,
      @NO,
      @NO,
      @YES
      ];
    //disabling empty cells
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    //disabling scroll
    self.tableView.alwaysBounceVertical = NO;
    self.tableView.scrollEnabled = NO;
    
    //disable muptiple selections:
    self.tableView.allowsMultipleSelection = NO;
    
    self.tableView.allowsSelection = NO;
    
    //initializing ProfilePartVC
    if (!self.profilePartVC) {
        self.profilePartVC = [BUProfilePartVC storyboardInstance];
    }
    
    
    
    
    //DONE: gragient from 66 to 54 in navBarBGVew
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.navBarBGVew.bounds;
    gradient.colors = @[(id)[UIColor menuLogoGradientStartColor].CGColor, (id)[UIColor menuLogoGradientStartColor].CGColor];
    
    [self.navBarBGVew.layer insertSublayer:gradient atIndex:0];
    
    
    //DONE: disable separator
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    if (![self.childViewControllers containsObject:self.profilePartVC])
    {
        //Adding ProfilePartVC
        [self addChildViewController:self.profilePartVC];
        [self.profilePartVC.view setFrame:self.profilePartPlaceholderView.bounds];
        
        [self.profilePartPlaceholderView addSubview:self.profilePartVC.view];
        [self.profilePartVC didMoveToParentViewController:self];
    }
}

#pragma mark - TableView
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return 4;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return  1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"menuCell" forIndexPath:indexPath];
    
    
    cell.backgroundColor = [UIColor menuSeparatorColor];
    
    UILabel *punctLabel = [cell viewWithTag:77];
    
    UIImageView *punctImage = [cell viewWithTag:66];
    
    UIImageView *checkImage = [cell viewWithTag:88];
    
    UIView *bottomSeparatorView = [cell viewWithTag:9];
    
    
    [cell.contentView bringSubviewToFront:bottomSeparatorView];
    
    //puncts
    punctLabel.text = _title[indexPath.row];
    
    punctImage.image = _image[indexPath.row];
    
    
    //selection color
    UIView *bgColorView = [[UIView alloc] initWithFrame:cell.frame];
    
    bgColorView.backgroundColor = [UIColor menuSelectedPunctColor];
    
    CGFloat cellHeight = cell.frame.size.height;
    CGFloat cellWidth = cell.frame.size.width;
    
    CGRect separatorFrame = CGRectMake(0, cellHeight - 1.0f, cellWidth, 1.0f);
    
    UIView *bgColorSeparatorView = [[UIView alloc] initWithFrame:separatorFrame];
    
    bgColorSeparatorView.backgroundColor = [UIColor menuSeparatorColor];
    
    [bgColorView addSubview:bgColorSeparatorView];
    
    
    
    [cell setSelectedBackgroundView:bgColorView];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleDefault];
    
    // Gives black background for you
    
    if (_enabled[indexPath.row].boolValue) {
        checkImage.hidden = NO;
        punctImage.alpha = 1.f;
        punctLabel.alpha = 1.f;
    } else {
        checkImage.hidden = YES;
        punctImage.alpha = 0.55f;
        punctLabel.alpha = 0.55f;
    }
    
    if (indexPath.row == _enabled.count-1) {
        checkImage.hidden = YES;
    }
    
    return cell;
}

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    // Remove seperator inset
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//
//    // Prevent the cell from inheriting the Table View's margin settings
//    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
//        [cell setPreservesSuperviewLayoutMargins:NO];
//    }
//
//    // Explictly set your cell's layout margins
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//}

//- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//
//    UIView *topSeparatorView = [cell viewWithTag:6];
//    UIView *bottomSeparatorView = [cell viewWithTag:9];
//
//    topSeparatorView.hidden = YES;
//    [cell.contentView bringSubviewToFront:bottomSeparatorView];
//    return indexPath;
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 3) {
        [BUDATAMANAGER logout];
    }
}
@end
