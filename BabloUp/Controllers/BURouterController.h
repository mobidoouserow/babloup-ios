//
//  BURouterController.h
//  BabloUp
//
//  Created by Pavel Wasilenko on 08/04/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import <UIKit/UIKit.h>

//#import <VK-ios-sdk/VKSdk.h>

#if HAS_POD(LGSideMenuController)
#import <LGSideMenuController/LGSideMenuController.h>
#import <LGSideMenuController/UIViewController+LGSideMenuController.h>
#endif

@interface BURouterController : NSObject

@property (strong, nonatomic) UINavigationController *navigationController;

@property (strong, nonatomic) LGSideMenuController *sideMenuController;

- (void)showInitialViewController;

- (BOOL)isLoginShowing;

- (void)hideMenu;
- (void)showMenu;

- (void)menuToggle;

- (void)showOffers;
- (void)showAccount;
- (void)showCoins;
- (void)showPartners;

@end
