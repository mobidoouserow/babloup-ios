//
//  BUCoinsVC.m
//  BabloUp
//
//  Created by Paul V. @bis_nival @telegram on 3/24/17
//  Copyright (c) 2014 Bars-i-Lis. All rights reserved.
//

#import "BUCoinsVC.h"

#import "BUCoinsVCHeaderCell.h"
#import "BUCoinsVCPayoutCell.h"
#import "BUCoinsVCPayoutsHeaderCell.h"

#import "BUPayout.h"

static NSString *coinsVCHeaderCellIdentifier = @"BUCoinsVCHeaderCell";
static NSString *coinsVCOfferCellIdentifier = @"BUCoinsVCPayoutCell";

static NSString *extension = @"payout-order";

static NSString *coinsVCOfferHeaderCellIdentifier = @"BUCoinsVCPayoutsHeaderCell";

@interface BUCoinsVC () {
    
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) CGFloat screenWidth;

@end


@implementation BUCoinsVC

#pragma mark - Lifecycle

- (void)viewDidLoad{
    [super viewDidLoad];
    ////////////////////////
    // DATABASE
    ////////////////////////
    
//    _status = ApiClientOfferStatusNew;
    
    //    YapDatabase *database = BUDATAMANAGER.database;
    
    //    databaseConnection = [database newConnection];
    //    databaseConnection.objectCacheLimit = 500;
    //    databaseConnection.metadataCacheEnabled = NO; // not using metadata in this example
    
    // What is a long-lived read transaction?
    // https://github.com/yaptv/YapDatabase/wiki/LongLivedReadTransactions
    
    [BUDATAMANAGER.uiConnection beginLongLivedReadTransaction];
    [BUDATAMANAGER initializeMappings];
    
    // What is YapDatabaseModifiedNotification?
    // https://github.com/yaptv/YapDatabase/wiki/YapDatabaseModifiedNotification
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(databaseModified:)
                                                 name:YapDatabaseModifiedNotification
                                               object:BUDATAMANAGER.database];
    
    ////////////////////////
    // UI + TABLE
    ////////////////////////

    self.screenWidth = CGRectGetWidth(self.view.bounds); //???
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.estimatedRowHeight = 400.0; //???
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    //DONE: disable separator
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    //disable muptiple selections:
    self.tableView.allowsMultipleSelection = NO;
    
    //disabling empty cells
//    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectZero];
    
    //registering cell's classes
    [self.tableView registerNib:[UINib nibWithNibName:@"BUCoinsVCHeaderCell" bundle:nil] forCellReuseIdentifier:coinsVCHeaderCellIdentifier];
//    [self.tableView registerNib:[UINib nibWithNibName:@"BUCoinsVCPayoutsHeaderCell" bundle:nil] forCellReuseIdentifier:coinsVCOfferHeaderCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"BUCoinsVCPayoutCell" bundle:nil] forCellReuseIdentifier:coinsVCOfferCellIdentifier];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [BUDATAMANAGER loadCoins];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.tableView reloadData];
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Database
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)databaseModified:(NSNotification *)ignored
{
    // This method is invoked (on the main-thread) after a readWriteTransaction (commit) has completed.
    // The commit could come from any databaseConnection (from our database).
    
    // Our databaseConnection is "frozen" on a particular commit. (via longLivedReadTransaction)
    // We do this in order to provide a stable data source for the UI.
    //
    // What we're going to do now is move our connection from it's current commit to the latest commit.
    // This is done atomically.
    // And we may jump multiple commits when we make this jump.
    // The notifications array gives us the details on each commit.
    //
    // This process/architecture is detailed in the following wiki articles:
    //
    // https://github.com/yaptv/YapDatabase/wiki/LongLivedReadTransactions
    // https://github.com/yaptv/YapDatabase/wiki/YapDatabaseModifiedNotification
    
    NSArray *notifications = [BUDATAMANAGER.uiConnection beginLongLivedReadTransaction];
    
    // Now that we've updated our "frozen" databaseConnection to the latest commit,
    // we need to update the tableViews (tableView & searchResultsTableView).
    //
    // Note: The code below is pretty much boiler-plate update code:
    // https://github.com/yaptv/YapDatabase/wiki/Views#full_animation_example
    //
    // The only difference here is that we have 2 tableViews to update (instead of just 1).
    
    if ([notifications count] == 0) {
        return;
    }
    
    NSArray *rowChanges = nil;
    
    // Update tableView
    
    
    
    DDLogVerbose(@"Calculating rowChanges for tableView...");
    
//    NSString *extension = @"payout-order"; //@"offer-order-new";
//    YapDatabaseViewMappings *mappings =  payMappings;// newMappings;
    
    
    [[BUDATAMANAGER.uiConnection ext:@"payout-order"] getSectionChanges:NULL
                                                       rowChanges:&rowChanges
                                                 forNotifications:notifications
                                                     withMappings:BUDATAMANAGER.payMappings];
    
    DDLogVerbose(@"Processing rowChanges for tableView...");
    
    if ([rowChanges count] > 0)
    {
        [_tableView beginUpdates];
        
        for (YapDatabaseViewRowChange *rowChange in rowChanges)
        {
            NSIndexPath *fixedIndexPath = [NSIndexPath indexPathForRow:rowChange.indexPath.row inSection:1];
            NSIndexPath *fixedNewIndexPath = [NSIndexPath indexPathForRow:rowChange.newIndexPath.row inSection:1];
            
            switch (rowChange.type)
            {
                case YapDatabaseViewChangeDelete :
                {
                    [_tableView deleteRowsAtIndexPaths:@[ fixedIndexPath ]
                                      withRowAnimation:UITableViewRowAnimationAutomatic];
                    break;
                }
                case YapDatabaseViewChangeInsert :
                {
                    [_tableView insertRowsAtIndexPaths:@[ fixedNewIndexPath ]
                                      withRowAnimation:UITableViewRowAnimationAutomatic];
                    break;
                }
                case YapDatabaseViewChangeMove :
                {
                    [_tableView deleteRowsAtIndexPaths:@[ fixedIndexPath ]
                                      withRowAnimation:UITableViewRowAnimationAutomatic];
                    [_tableView insertRowsAtIndexPaths:@[ fixedNewIndexPath ]
                                      withRowAnimation:UITableViewRowAnimationAutomatic];
                    break;
                }
                case YapDatabaseViewChangeUpdate :
                {
                    [_tableView reloadRowsAtIndexPaths:@[ fixedIndexPath ]
                                      withRowAnimation:UITableViewRowAnimationNone];
                    break;
                }
            }
        }
        
        [_tableView endUpdates];
    }
}

#pragma mark - Actions

- (IBAction)menuButtonClick:(id)sender {
    [[BUDataManager instance].router menuToggle];
}

//TODO: add actions for header QIWI - phone - WM (? or add actions in header ?)


#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if (section == 0) {
        return 1;//секция для колва монет и оплаты
    } else {
        NSUInteger count = [BUDATAMANAGER.payMappings numberOfItemsInGroup:@"payout-group"];//TODO: секция для транзакций - считать количество BUPayments всего
        return count;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 2;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;
{
    if (section == 1) {
        UIView *headerView = [[[NSBundle mainBundle] loadNibNamed:@"BUCoinsVCPayoutsHeaderCell" owner:self options:nil] lastObject];
        CGRect newFrame = headerView.frame;
        newFrame.size.width = self.screenWidth;
        [headerView setFrame:newFrame];
        return headerView;
        
    } else {
        return [[UIView alloc] initWithFrame:CGRectZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        return 44; //нужно сделать это автоматическим
    } else {
        return 0.;
    }
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
    self.screenWidth = CGRectGetWidth(self.view.bounds);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if ([indexPath row] == 0 && [indexPath section] == 0) {
//        return 500;
//    }
    
    DDLogInfo(@"Section %ld Row %ld", (long)[indexPath section], (long)[indexPath row]);
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if ([indexPath section] == 0) {
        BUCoinsVCHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:coinsVCHeaderCellIdentifier];
        if (!cell) {
            cell = [[BUCoinsVCHeaderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:coinsVCHeaderCellIdentifier];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.parentVC = self;
        return cell;
    } else

    if ([indexPath section] == 1) {
        BUCoinsVCPayoutCell *cell = [tableView dequeueReusableCellWithIdentifier:coinsVCOfferCellIdentifier];
        if (!cell) {
            cell = [[BUCoinsVCPayoutCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:coinsVCOfferCellIdentifier];
        }
        
        //TODO: fill cell with BUPayment
        __block BUPayout *payout = nil;

        [BUDATAMANAGER.uiConnection readWithBlock:^(YapDatabaseReadTransaction *transaction) {
            extension = @"payout-order";
            
            NSIndexPath *idx = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
            
            payout = [[transaction ext:extension] objectAtIndexPath:idx withMappings:BUDATAMANAGER.payMappings];
        }];

        cell.tableView = self.tableView;
        cell.indexPath = indexPath;
        
        cell.model = payout;
        
        [cell configure];
        
        return cell;
        
    }
    
    return [[UITableViewCell alloc] initWithFrame:CGRectZero];;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        return indexPath;
    } else {
        return nil;
    }
}


//self.layer.shadowColor = [UIColor greenColor].CGColor;
//self.layer.shadowOpacity = 0.8;
//self.layer.shadowRadius = 4;
//self.layer.shadowOffset = CGSizeMake(12.0f, 12.0f);


@end
