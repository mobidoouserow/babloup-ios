//
//  BUOfferTableViewCell.m
//  BabloUp
//
//  Created by Pavel Wasilenko on 06.05.17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import "BUOfferTableViewCell.h"
#import <UIImageView+AFNetworking.h>
#import "BUOffer.h"

@interface BUOfferTableViewCell ()

@property (weak, nonatomic) IBOutlet BUBaseView *roundedContentView;

@property (weak, nonatomic) IBOutlet BUBaseView *offerImageContainerView;
@property (weak, nonatomic) IBOutlet UIImageView *offerImageView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLable;
@property (weak, nonatomic) IBOutlet UIImageView *checkDownloadImageView;

@property (weak, nonatomic) IBOutlet UIView *verticalSeparatorView;
@property (weak, nonatomic) IBOutlet UIImageView *coinsImageView;
@property (weak, nonatomic) IBOutlet UILabel *coinsLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *subtitleLabelConstraint;

@end

@implementation BUOfferTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];

    // Configure the view for the selected state

//    __weak typeof(self) weakSelf = self;
    void (^animateSelect)(void) = ^void(void)
    {
//        __strong typeof(self) strongSelf = weakSelf;
        self.roundedContentView.backgroundColor = [UIColor bu_dirtBrownColor];
//        self.offerImageContainerView.layer.borderColor = [[UIColor avatarSelectedBorderColor] CGColor];
    };
    
    __weak typeof(self) weakSelf = self;    
    void (^animateUnSelect)(void) = ^void(void)
    {
        __strong typeof(self) strongSelf = weakSelf;
        BUOffer *off = (BUOffer *)strongSelf.model;
        
        if (off.statusEnum && off.statusEnum == ApiClientOfferStatusUndefined) {
            self.roundedContentView.backgroundColor = [UIColor bu_darkBlueGreenColor];
        } else {
            self.roundedContentView.backgroundColor = [UIColor bu_blackTwoColor];
        }
//        self.offerImageContainerView.layer.borderColor = [[UIColor avatarBorderColor] CGColor];
        
    };
    
    if (animated) {
        [UIView animateWithDuration:STANDARD_ANIMATION_DURATION
                         animations: selected ? animateSelect : animateUnSelect
                         completion:nil];
    } else {
        if (selected) {
            animateSelect();
        } else {
            animateUnSelect();
        }
    }
}

-(void)prepareForReuse {
    //TODO: nullify all
    self.checkDownloadImageView.hidden = NO;
    self.subtitleLabelConstraint.constant = 27.0;
    
    self.checkDownloadImageView.image = [UIImage imageNamed:@"Offers-CheckDownload"];
    
    [self.offerImageView cancelImageDownloadTask];
    [self.offerImageView setImage:[UIImage imageNamed:@"Menu-Account"]];
    
    self.offerImageView.backgroundColor = [UIColor clearColor];
    self.offerImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    self.titleLabel.text = @"";
    
    self.subTitleLable.text = @"";
    
    self.coinsLabel.text = @"";
    
    self.verticalSeparatorView.hidden = NO;
    self.coinsLabel.hidden = NO;
    self.coinsImageView.hidden = NO;
    
    self.coinsImageView.image = [UIImage imageNamed:@"Offers-Offer-Coins"];
    
    self.roundedContentView.backgroundColor = [UIColor bu_blackTwoColor];
    [self.roundedContentView setNeedsLayout];
    
    self.offerImageContainerView.borderWidth = 2.0f;
    [self.offerImageContainerView setNeedsLayout];
}

//- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;
//{
//    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
//        
//    }
//}

    /*
    {
        "created_at" = "2017-05-30 08:52:15";
        id = 20;
        link = "http://mobidoo-tracker.dev/click?offer_id=20&pid=17";
        logo = "http://lorempixel.com/200/200/?24382";
        package = "<null>";
        revenue = 32;
        status = "<null>";
        "task_description" =             (
                                          "Deserunt officiis rerum cumque iure deserunt veniam incidunt",
                                          "Praesentium est sed quia",
                                          "Quasi quo quam dolore et aut nihil possimus",
                                          "Aliquam voluptatum sit animi debitis",
                                          "Explicabo quis eos ratione dolorum",
                                          "Perferendis aliquam quam sed aut consequuntur"
                                          );
        "task_type" = install;
        title = "Horizontal hybrid localareanetwork";
    },
    {
        "created_at" = "2017-05-30 08:52:15";
        id = 21;
        link = "http://mobidoo-tracker.dev/click?offer_id=21&pid=17";
        logo = "http://lorempixel.com/200/200/?46351";
        package = "<null>";
        revenue = 96;
        status = "<null>";
        "task_description" =             (
                                          "Neque aut quia corporis",
                                          "Numquam deleniti sed maxime fuga voluptatum molestiae voluptatem",
                                          "Tenetur nihil laborum exercitationem temporibus incidunt",
                                          "Voluptatem dolor occaecati eaque illo quod fuga voluptatem",
                                          "Cupiditate alias consequuntur molestiae autem omnis praesentium voluptatem"
                                          );
        "task_type" = task;
        title = "Centralized tangible localareanetwork";
    },
    */

- (void)configure;
{
    //    NSIndexPath *indexPath = [self.tableView indexPathForCell:self];
    
    BUOffer *offer = (BUOffer *)self.model;
    
    self.titleLabel.text = offer.title;
    self.subTitleLable.text = [offer.task_description firstObject];
    self.backgroundColor = [UIColor bu_blackColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    // Конфиг для обычных офферов
    if (offer.statusEnum != ApiClientOfferStatusUndefined) {
        self.checkDownloadImageView.hidden = NO;
        self.subtitleLabelConstraint.constant = 27.0;
        
        self.roundedContentView.backgroundColor = [UIColor bu_blackTwoColor];
        [self.roundedContentView setNeedsLayout];
        
        self.offerImageContainerView.borderWidth = 2.0f;
        [self.offerImageContainerView setNeedsLayout];
        
        self.coinsImageView.image = [UIImage imageNamed:@"Offers-Offer-Coins"];
        
        if ([offer.task_type isEqualToString:@"install"]) {
            self.checkDownloadImageView.image = [UIImage imageNamed:@"Offers-CheckDownload"];
        } else {
            self.checkDownloadImageView.image = [UIImage imageNamed:@"Offers-CheckMark"];
        }
        
        NSURL *url = [NSURL URLWithString:offer.logo];
        
        if (url) {
            [self.offerImageView setImageWithURL:url
                                placeholderImage:[UIImage imageNamed:@"Menu-Account"]];
        }

        self.subTitleLable.numberOfLines = 1;
        NSNumber *amount = offer.revenue;
        self.coinsLabel.text = [NSString stringWithFormat:@"%@", amount];
        
        //#warning Comment this after IIOS-25 completion
        //    if (offer.package.length) {
        //        self.titleLable.textColor = [UIColor redColor];
        //    } else {
        //        self.titleLable.textColor = [UIColor whiteColor];
        //    }
    } else {
        
        //Конфиг для Видео Дня / Задание от партнёра
        self.checkDownloadImageView.hidden = YES;
        self.subtitleLabelConstraint.constant = 12.0;
        
        self.roundedContentView.backgroundColor = [UIColor bu_darkBlueGreenColor];
        [self.roundedContentView setNeedsLayout];
        
        self.offerImageContainerView.borderWidth = 0.0f;
        [self.offerImageContainerView setNeedsLayout];
        
        self.coinsImageView.image = [UIImage imageNamed:@"Offers-VideoCoin"];
        
        // Видео Дня
        if ([offer.title isEqualToString:@"Видео Дня"]) {
            self.offerImageView.image = [UIImage imageNamed:@"Offers-VideoPlay"];

            if (BUDATAMANAGER.currentUser.video_revenue) {
                self.coinsLabel.text = [NSString stringWithFormat:@"%@", BUDATAMANAGER.currentUser.video_revenue];
            } else {
                self.coinsLabel.text = @"0.1";
            }
        }
        else // Предложения Партнёров
        {
            self.offerImageView.backgroundColor = [UIColor lightGrayColor];
            self.offerImageView.contentMode = UIViewContentModeScaleAspectFit;
            
            self.verticalSeparatorView.hidden = YES;
            self.coinsLabel.hidden = YES;
            self.coinsImageView.hidden = YES;
            
            NSURL *logoUrl = [NSURL URLWithString:offer.logo];
            
            [self.offerImageView setImageWithURL:logoUrl
                                placeholderImage:[UIImage imageNamed:@"Offers-Partners"]];
            
//            if (BUDATAMANAGER.currentUser.offerwall_revenue) {
//                self.coinsLabel.text = [NSString stringWithFormat:@"%@", BUDATAMANAGER.currentUser.offerwall_revenue];
//            } else {
//                self.coinsLabel.text = @"5";
//            }
        }
        
        self.subTitleLable.numberOfLines = 2;
    }
    
}

@end
