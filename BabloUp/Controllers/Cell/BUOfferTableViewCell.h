//
//  BUOfferTableViewCell.h
//  BabloUp
//
//  Created by Pavel Wasilenko on 06.05.17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BUOfferTableViewCell : UITableViewCell

//@property (nonatomic, weak) UITableView *tableView;
//@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, strong) NSObject *model;

- (void)configure;

@end
