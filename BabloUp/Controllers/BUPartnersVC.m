//
//  BUPartnersVC.m
//  BabloUp
//
//  Created by Paul V. @bis_nival @telegram on 3/24/17
//  Copyright (c) 2014 Bars-i-Lis. All rights reserved.
//

#import "BUPartnersVC.h"

#import "BUReferralCodeEnterCell.h"
#import "BUReferralCell.h"
#import "BUReferralIncomeCell.h"
#import "BUReferralInviteCodeCell.h"

static NSString *partnersReferralCodeEnterCellIdentifier = @"BUReferralCodeEnterCell";
static NSString *partnersReferralCellIdentifier = @"BUReferralCell";
static NSString *partnersReferralsHeaderCellIdentifier = @"BUReferralsHeaderCell";
static NSString *partnersReferralIncomeHeaderCellIdentifier = @"BUReferralIncomeHeaderCell";
static NSString *partnersReferralInviteCodeCellIdentifier = @"BUReferralInviteCodeCell";
static NSString *partnersReferralIncomeCellIdentifier = @"BUReferralIncomeCell";

@interface BUPartnersVC ()

@property (weak, nonatomic) IBOutlet UITableView *mainTableView;

@end


@implementation BUPartnersVC

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.estimatedRowHeight = 400.0;
    self.mainTableView.rowHeight = UITableViewAutomaticDimension;
    
    //DONE: disable separator
    self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    //disable muptiple selections:
    self.mainTableView.allowsMultipleSelection = NO;
    
    //disabling empty cells
    //    self.mainTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    //    self.mainTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self.mainTableView setContentInset:UIEdgeInsetsMake(0., 0., 68., 0.)];
    
    [self.mainTableView registerNib:[UINib nibWithNibName:@"BUReferralCodeEnterCell" bundle:nil] forCellReuseIdentifier:partnersReferralCodeEnterCellIdentifier];
    
    [self.mainTableView registerNib:[UINib nibWithNibName:@"BUReferralCell" bundle:nil] forCellReuseIdentifier:partnersReferralCellIdentifier];
    [self.mainTableView registerNib:[UINib nibWithNibName:@"BUReferralsHeaderCell" bundle:nil] forCellReuseIdentifier:partnersReferralsHeaderCellIdentifier];
    [self.mainTableView registerNib:[UINib nibWithNibName:@"BUReferralIncomeHeaderCell" bundle:nil] forCellReuseIdentifier:partnersReferralIncomeHeaderCellIdentifier];
    [self.mainTableView registerNib:[UINib nibWithNibName:@"BUReferralInviteCodeCell" bundle:nil] forCellReuseIdentifier:partnersReferralInviteCodeCellIdentifier];
    [self.mainTableView registerNib:[UINib nibWithNibName:@"BUReferralIncomeCell" bundle:nil] forCellReuseIdentifier:partnersReferralIncomeCellIdentifier];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.mainTableView reloadData];
}

- (IBAction)menuButtonClick:(id)sender {
    [[BUDataManager instance].router menuToggle];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    switch (section) {
        case 0: //секция для инвайт-кода
            return 2;
            break;
        case 1: //секция для дохода от рефералов
            return 1;
            break;
        case 2: //секция рефералов
            return 0;
            break;
        default:
            return 0;
            break;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 4;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;
{
    switch (section) {
        case 1: //секция для дохода от рефералов
            return [[[NSBundle mainBundle] loadNibNamed:@"BUReferralIncomeHeaderCell" owner:self options:nil] lastObject];;
            break;
        case 2: //секция рефералов
            return [[[NSBundle mainBundle] loadNibNamed:@"BUReferralsHeaderCell" owner:self options:nil] lastObject];;
            break;
        default:
            return [[UIView alloc] initWithFrame:CGRectZero];;
            break;
    }
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ((section == 1) || (section == 2)) {
        return 22; //нужно сделать это автоматическим
    } else {
        return 0.;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //    if ([indexPath row] == 0 && [indexPath section] == 0) {
    //        return 500;
    //    }
    
//    NSLog(@"Section %ld Row %ld", (long)[indexPath section], (long)[indexPath row]);
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    UITableViewCell *cell;
    
    if ([indexPath section] == 0 && [indexPath row] == 0 ) {
        cell = [tableView dequeueReusableCellWithIdentifier:partnersReferralCodeEnterCellIdentifier];
        if (!cell) {
            cell = [[BUReferralCodeEnterCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:partnersReferralCodeEnterCellIdentifier];
        }
    }
    
    if ([indexPath section] == 0 && [indexPath row] == 1 ) {
        cell = [tableView dequeueReusableCellWithIdentifier:partnersReferralInviteCodeCellIdentifier];
        if (!cell) {
            cell = [[BUReferralInviteCodeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:partnersReferralInviteCodeCellIdentifier];
        }
    }
    
    if ([indexPath section] == 1 && [indexPath row] == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:partnersReferralIncomeCellIdentifier];
        if (!cell) {
            cell = [[BUReferralIncomeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:partnersReferralIncomeCellIdentifier];
        }
    }
    
    if ([indexPath section] == 2 && [indexPath row] == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:partnersReferralCellIdentifier];
        if (!cell) {
            cell = [[BUReferralCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:partnersReferralCellIdentifier];
        }
    }
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithFrame:CGRectZero];;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}




@end
