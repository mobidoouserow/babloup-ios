//
//  BUReferralInviteCodeCell.m
//  BabloUp
//
//  Created by John FrostFox on 14/05/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import "BUReferralInviteCodeCell.h"

@interface BUReferralInviteCodeCell ()

@property (weak, nonatomic) IBOutlet UILabel *referralCodeLabel;

@end

@implementation BUReferralInviteCodeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userUpdated) name:USER_UPDATED_NOTIFICATION object:nil];
    
    [self userUpdated];
}

-(void)userUpdated;
{
    _referralCodeLabel.text = BUDATAMANAGER.currentUser.referral_code;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)inviteReferralClick:(id)sender {
    //TODO: реализуем ActionSheet или стандартный шаринг-диалог.
    
    
    NSMutableArray *shareItems = [@[
                                  [NSString stringWithFormat:@"Регистрируйтейсь в новой программе! Вводите мой код %@ и получайте привилегии!", BUDATAMANAGER.currentUser.referral_code],
                                  [NSURL URLWithString:@"http://babloapp.ru/"]] mutableCopy];
    [self shareItemToOtherApp:shareItems];
    
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:
//                                                                   message:message
//                                                            preferredStyle:UIAlertControllerStyleAlert];
//    
//    UIAlertAction *ok =
//    [UIAlertAction actionWithTitle:@"Ok"
//                             style:UIAlertActionStyleDefault
//                           handler:^(UIAlertAction * _Nonnull action) {
//                               ;
//                           }];
//    [alert addAction:ok];
//    
//    //done: router - current view controller
//    [self.router.navigationController.topViewController presentViewController:alert animated:YES completion:nil];
    
}


-(void)shareItemToOtherApp:(NSMutableArray *)shareItems{
    UIActivityViewController *shareController = [[UIActivityViewController alloc]
                                                 initWithActivityItems: shareItems applicationActivities :nil];
    
    [shareController setValue:@"Sharing" forKey:@"subject"];
    shareController.excludedActivityTypes = @[UIActivityTypePostToWeibo, UIActivityTypeAssignToContact, UIActivityTypePrint, UIActivityTypeSaveToCameraRoll];
    
    shareController.completionWithItemsHandler = ^(UIActivityType  _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError)
    {
        DDLogInfo(@" error: %@", activityError);
        DDLogInfo(@" activityType: %@", activityType);
        DDLogInfo(@" completed: %i", completed);
        DDLogInfo(@" returned items : %@", returnedItems);
    };
    
    [BUDATAMANAGER.router.navigationController.topViewController presentViewController:shareController animated:YES completion: nil];
}

@end
