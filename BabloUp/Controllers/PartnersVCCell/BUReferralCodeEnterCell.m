//
//  BUReferralCodeEnterCell.m
//  BabloUp
//
//  Created by John FrostFox on 14/05/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import "BUReferralCodeEnterCell.h"

@interface BUReferralCodeEnterCell ()

@property (weak, nonatomic) IBOutlet UITextField *referrerTextField;
@property (weak, nonatomic) IBOutlet UIButton *registerReferrerCodeButton;

@end

@implementation BUReferralCodeEnterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userUpdated) name:USER_UPDATED_NOTIFICATION object:nil];
    
    [self userUpdated];
}

-(void)userUpdated;
{
    if (BUDATAMANAGER.currentUser.referrer_code.length) {
        self.referrerTextField.text = BUDATAMANAGER.currentUser.referrer_code;
        self.referrerTextField.userInteractionEnabled = NO;
        self.registerReferrerCodeButton.userInteractionEnabled = NO;
    } else {
        self.referrerTextField.text = @"";
        self.referrerTextField.userInteractionEnabled = YES;
        self.registerReferrerCodeButton.userInteractionEnabled = YES;
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)inviteReferralClick:(id)sender {
    NSString * referrer = [self.referrerTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (!referrer
        || referrer.length != 6
        )
    {
        [BUDATAMANAGER showAlertErrorWithMessage:@"Неверный код реферрера!"];
    }
    
    //TODO: отправка кода реферрера
    [BUDATAMANAGER saveReferrer:referrer];
}

@end
