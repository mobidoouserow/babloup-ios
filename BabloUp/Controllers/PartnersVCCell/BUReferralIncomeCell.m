//
//  BUreferralIncomeCell.m
//  BabloUp
//
//  Created by John FrostFox on 14/05/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import "BUReferralIncomeCell.h"

@interface BUReferralIncomeCell ()

@property (weak, nonatomic) IBOutlet UILabel *referralsCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *referralsEarningsLabel;

@end

@implementation BUReferralIncomeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userUpdated) name:USER_UPDATED_NOTIFICATION object:nil];
    
    [self userUpdated];
}

-(void)userUpdated;
{
    _referralsCountLabel.text = [NSString stringWithFormat:@"%@", BUDATAMANAGER.currentUser.referrals_amount];
    _referralsEarningsLabel.text = [NSString stringWithFormat:@"%@", BUDATAMANAGER.currentUser.referrals_earnings];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc;
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
