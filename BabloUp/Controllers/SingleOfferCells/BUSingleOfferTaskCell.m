//
//  BUSingleOfferTaskCell.m
//  BabloUp
//
//  Created by John FrostFox on 01/06/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import "BUSingleOfferTaskCell.h"

#import "BUOffer.h"

#import <UIImageView+AFNetworking.h>

@interface BUSingleOfferTaskCell ()

@property (weak, nonatomic) IBOutlet UIImageView *offerImageView;
@property (weak, nonatomic) IBOutlet UILabel *offerTitleLabel;

@end

@implementation BUSingleOfferTaskCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configure {
    self.offerTitleLabel.text = self.offer.title;
    
    NSURL *imageURL = [NSURL URLWithString:self.offer.logo];
    
    if (imageURL) {
        [self.offerImageView setImageWithURL:imageURL
                            placeholderImage:[UIImage imageNamed:@"Menu-Account"]];
    }
}

@end
