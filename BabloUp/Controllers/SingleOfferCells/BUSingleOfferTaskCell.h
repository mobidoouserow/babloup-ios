//
//  BUSingleOfferTaskCell.h
//  BabloUp
//
//  Created by John FrostFox on 01/06/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BUOffer;

@interface BUSingleOfferTaskCell : UIView

@property BUOffer *offer;

-(void)configure;

@end
