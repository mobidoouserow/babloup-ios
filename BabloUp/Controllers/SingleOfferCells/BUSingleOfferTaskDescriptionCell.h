//
//  BUSingleOfferTaskDescriptionCell.h
//  BabloUp
//
//  Created by John FrostFox on 02/06/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BUSingleOfferTaskDescriptionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end
