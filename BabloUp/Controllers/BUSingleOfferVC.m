//
//  BUSingleOfferVC.m
//  BabloUp
//
//  Created by Paul V. @bis_nival @telegram on 4/1/17
//  Copyright (c) 2017 Bars-i-Lis. All rights reserved.
//

#import "BUSingleOfferVC.h"
#import "BUSingleOfferTaskCell.h"
#import "BUSingleOfferTaskDescriptionCell.h"

#import "BUOffer.h"

static NSString *singleOfferTaskCellIdentifier = @"BUSingleOfferTaskCell";
static NSString *singleOfferTaskDescriptionCellIdentifier = @"BUSingleOfferTaskDescriptionCell";
static NSString *singleOfferTaskFooterCellIdentifier = @"BUSingleOfferTaskFooterCell";

@interface BUSingleOfferVC () <UITableViewDelegate, UITableViewDataSource>
{
    
}

@property (weak, nonatomic) IBOutlet UIView *navBarBGVew;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *getCoinsButton;

@property (weak, nonatomic) BUSingleOfferTaskCell *taskView;

@end


@implementation BUSingleOfferVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
        
    //DONE: gragient from 66 to 54 in navBarBGVew
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.navBarBGVew.bounds;
    gradient.colors = @[(id)[UIColor menuLogoGradientStartColor].CGColor, (id)[UIColor menuLogoGradientStartColor].CGColor];
    
    [self.navBarBGVew.layer insertSublayer:gradient atIndex:0];
    
    
    
    //DONE: disable separator
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    //disable muptiple selections:
    self.tableView.allowsMultipleSelection = NO;
    
    //disabling empty cells
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.tableView.tableFooterView.hidden = YES;
    self.tableView.tableHeaderView.hidden = YES;
    
//    [self.tableView registerNib:[UINib nibWithNibName:@"BUSingleOfferTaskCell" bundle:nil] forCellReuseIdentifier:singleOfferTaskCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"BUSingleOfferTaskDescriptionCell" bundle:nil] forCellReuseIdentifier:singleOfferTaskDescriptionCellIdentifier];

//    self.tableView.bounces = NO;
//    [self.tableView setContentInset:UIEdgeInsetsMake(0., 0., 0., 0.)];
    //    [self.tableView setScrollIndicatorInsets:UIEdgeInsetsMake(36., 0., 20., 0.)];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.tableView.separatorColor = [UIColor clearColor];
//    self.tableView.rowHeight = UITableViewAutomaticDimension;
//    self.tableView.estimatedRowHeight = 60;
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self configure];
}

- (void)configure;
{
    [self.taskView configure];
    
    NSString *title = [NSString stringWithFormat:@"Получить %@ монет", self.offer.revenue];
    [self.getCoinsButton setTitle:title forState:UIControlStateNormal];
    [self.getCoinsButton setTitle:title forState:UIControlStateHighlighted];
    
    
//        //TODO: Uncomment
//    if (self.offer.statusEnum == ApiClientOfferStatusCompleted) {
//        self.getCoinsButton.enabled = NO;
//    }
    
    [self.tableView reloadData];
}

#pragma mark - Actions
- (IBAction)getCoinsButtonClick:(id)sender {
    //done: open safari with url
    if (self.offer.link.length || self.offer.package.length) {
        
        //done: toggle offer.status
        
//#warning IIOS-25 - testing- changing offer status - inverse comment on completion
//        
//        ApiClientOfferStatus status = self.offer.statusEnum;
//        NSString *key = self.offer.offer_id_str;
//        
//        __weak typeof(self) weakSelf = self;
//        [BUDATAMANAGER.bgConnection readWriteWithBlock:^(YapDatabaseReadWriteTransaction * _Nonnull transaction) {
//            __strong typeof(self) strongSelf = weakSelf;
//            
//            switch (status) {
//                case ApiClientOfferStatusNew:
//                    strongSelf.offer.statusEnum = ApiClientOfferStatusActive;
//                    break;
//                case ApiClientOfferStatusActive:
//                    strongSelf.offer.statusEnum = ApiClientOfferStatusCompleted;
//                    break;
//                case ApiClientOfferStatusCompleted:
//                    strongSelf.offer.statusEnum = ApiClientOfferStatusNew;
//                    break;
//                default:
//                    break;
//            }
//            
//            [transaction setObject:strongSelf.offer forKey:key inCollection:@"offer"];
//        }];
        
        //TODO: Uncomment
        NSURL *linkUrl = [NSURL URLWithString:self.offer.link];
        
        if (linkUrl) {
            if ([[UIApplication sharedApplication] canOpenURL:linkUrl]) {
                [[UIApplication sharedApplication] openURL:linkUrl];
            }
        }
    }
}



- (IBAction)menuButtonClick:(id)sender {
    //done: disiss SingleOffer
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    NSUInteger cnt = 0;
    if (self.offer && self.offer.task_description) {
        cnt = self.offer.task_description.count;
    }
    
    return cnt;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 1;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;
{
    self.taskView = [[[NSBundle mainBundle] loadNibNamed:@"BUSingleOfferTaskCell" owner:self options:nil] lastObject];
    self.taskView.offer = self.offer;
    
    [self.taskView configure];
    
    return self.taskView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
{
    return 84.0f;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section
{
    return 84.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 61.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    BUSingleOfferTaskDescriptionCell *cell = [tableView dequeueReusableCellWithIdentifier:singleOfferTaskDescriptionCellIdentifier];
    if (!cell) {
        cell = [[BUSingleOfferTaskDescriptionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:singleOfferTaskDescriptionCellIdentifier];
    }
    
    cell.numberLabel.text = [NSString stringWithFormat:@"%ld",(long)indexPath.row+1];
    cell.descriptionLabel.text = self.offer.task_description[indexPath.row];
    
    return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    //disallow select cell
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}


//- (IBAction)checkOfferButton:(id)sender {
//    DDLogInfo(@"offer = \n\n%@\n", self.offer);
//    
//    
//    [[BUDataManager instance] checkIfOfferCompleted:self.offer sendCompletion:NO];
//}

@end
