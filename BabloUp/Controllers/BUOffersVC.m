//
//  BUOffersVC.m
//  BabloUp
//
//  Created by Paul V. @bis_nival @telegram on 3/15/17
//  Copyright (c) 2014 Bars-i-Lis. All rights reserved.
//

#import "BUOffersVC.h"

#import "BUOffer.h"

#import "BUOfferTableViewCell.h"

#import "BUSingleOfferVC.h"

#import "FyberSDK.h"

#import <AppLovinSDK/AppLovinSDK.h>

#import "NativeXSDK.h"

#import <OfferToroSDK/OfferToroSDK.h>
#import <OfferToroSDK/OfferToro.h>

static NSString *offerCellIdentifier = @"BUOfferTableViewCell";

@interface BUOffersVC () <UITableViewDelegate, UITableViewDataSource, ALAdRewardDelegate, ALAdLoadDelegate, NativeXSDKDelegate, NativeXAdEventDelegate, OfferToroDelegate>
{
    ApiClientOfferStatus _status;
    BOOL _gradientMade;
    
    BOOL _partnersInitialized;
}

@property (weak, nonatomic) IBOutlet UILabel *coinsLabel;
@property (weak, nonatomic) IBOutlet UIView *navBarBGVew;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UITableView *createdTableView;
@property (weak, nonatomic) IBOutlet UITableView *activeTableView;
@property (weak, nonatomic) IBOutlet UITableView *completedTableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *statusSegmentedControl;


@property (strong, nonatomic) UIRefreshControl *createdRefreshControl;
@property (strong, nonatomic) UIRefreshControl *activeRefreshControl;
@property (strong, nonatomic) UIRefreshControl *completedRefreshControl;

@end


@implementation BUOffersVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //done: 1. make model
    //done: 2. mock model
    ////////////////////////
    // DATABASE
    ////////////////////////
    
    _status = ApiClientOfferStatusNew;
    
//    YapDatabase *database = BUDATAMANAGER.database;
    
//    databaseConnection = [database newConnection];
//    databaseConnection.objectCacheLimit = 500;
//    databaseConnection.metadataCacheEnabled = NO; // not using metadata in this example
    
    // What is a long-lived read transaction?
    // https://github.com/yaptv/YapDatabase/wiki/LongLivedReadTransactions
    
    [BUDATAMANAGER.uiConnection beginLongLivedReadTransaction];
    [BUDATAMANAGER initializeMappings];
    
    // What is YapDatabaseModifiedNotification?
    // https://github.com/yaptv/YapDatabase/wiki/YapDatabaseModifiedNotification
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(databaseModified:)
                                                 name:YapDatabaseModifiedNotification
                                               object:BUDATAMANAGER.database];
    
    ////////////////////////
    // UI + TABLE
    ////////////////////////
    NSNumber *coins = BUDATAMANAGER.currentUser.balance;
    
    NSString *coinsStr = coins.integerValue ? [NSString stringWithFormat:@"%@", coins] : @"0";
    
    self.coinsLabel.text = coinsStr;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userUpdated)
                                                 name:USER_UPDATED_NOTIFICATION
                                               object:nil];
    
    //
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.createdTableView.contentInset = UIEdgeInsetsZero;
    self.activeTableView.contentInset = UIEdgeInsetsZero;
    self.completedTableView.contentInset = UIEdgeInsetsZero;
    
    //disabling empty cells
    self.createdTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.createdTableView.tableFooterView.backgroundColor = [UIColor blueColor];
    self.createdTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectZero];
    self.createdTableView.tableHeaderView.backgroundColor = [UIColor purpleColor];
    
    //DONE: disable separator
    self.createdTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    //disable muptiple selections:
    self.createdTableView.allowsMultipleSelection = NO;
    
    [self.createdTableView registerNib:[UINib nibWithNibName:@"BUOfferTableViewCell" bundle:nil] forCellReuseIdentifier:offerCellIdentifier];
    
//    [self.createdTableView setContentInset:UIEdgeInsetsMake(-36., 0., -12., 0.)];
    //    [self.createdTableView setScrollIndicatorInsets:UIEdgeInsetsMake(36., 0., 20., 0.)];
    
    
    //DONE: disable separator
    self.activeTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    //disable muptiple selections:
    self.activeTableView.allowsMultipleSelection = NO;
    
    //disabling empty cells
    self.activeTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.activeTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self.activeTableView registerNib:[UINib nibWithNibName:@"BUOfferTableViewCell" bundle:nil] forCellReuseIdentifier:offerCellIdentifier];
    
//    [self.activeTableView setContentInset:UIEdgeInsetsMake(-36., 0., -12., 0.)];
    //    [self.activeTableView setScrollIndicatorInsets:UIEdgeInsetsMake(36., 0., 20., 0.)];
    
    
    
    //DONE: disable separator
    self.completedTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    //disable muptiple selections:
    self.completedTableView.allowsMultipleSelection = NO;
    
    //disabling empty cells
    self.completedTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.completedTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self.completedTableView registerNib:[UINib nibWithNibName:@"BUOfferTableViewCell" bundle:nil] forCellReuseIdentifier:offerCellIdentifier];
    
//    [self.completedTableView setContentInset:UIEdgeInsetsMake(-35., 0., -37., 0.)];
    //    [self.completedTableView setScrollIndicatorInsets:UIEdgeInsetsMake(36., 0., 20., 0.)];
    
//    self.createdTableView.contentInset = UIEdgeInsetsZero;
//    self.activeTableView.contentInset = UIEdgeInsetsZero;
//    self.completedTableView.contentInset = UIEdgeInsetsZero;
//    self.createdTableView.contentInset = UIEdgeInsetsMake(-35., 0., -38., 0.);
    
//    self.activeTableView.automaticallyAdjustsScrollViewInsets = NO;
    
    
    //TODO: remove after branching for SingleOffer
    
//    self.statusSegmentedControl.userInteractionEnabled = NO;
    
//    _status = ApiClientOfferStatusNew;
//    self.createdTableView.hidden = NO;
//    self.activeTableView.hidden = YES;
//    self.completedTableView.hidden = YES;
    
    self.createdRefreshControl = [[UIRefreshControl alloc] init];
    self.activeRefreshControl = [[UIRefreshControl alloc] init];
    self.completedRefreshControl = [[UIRefreshControl alloc] init];
    
    [self.createdTableView addSubview:self.createdRefreshControl];
    [self.activeTableView addSubview:self.activeRefreshControl];
    [self.completedTableView addSubview:self.completedRefreshControl];
    
    [self.createdRefreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    [self.activeRefreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    [self.completedRefreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadedOffers:)
                                                 name:OFFERS_LOADED_NOTIFICATION
                                               object:nil];
    
    
    [self.statusSegmentedControl setSelectedSegmentIndex:0];
    [self segmentedClick:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (!_gradientMade) {
        //DONE: gragient from 66 to 54 in navBarBGVew
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = self.navBarBGVew.bounds;
        gradient.colors = @[(id)[UIColor menuLogoGradientStartColor].CGColor, (id)[UIColor menuLogoGradientStartColor].CGColor];
        
        [self.navBarBGVew.layer insertSublayer:gradient atIndex:0];
        
        _gradientMade = YES;
    }
    
    //done: дёргать метод загрузки Offers
//    [BUDATAMANAGER loadOffersOfType:_status];
    
//    [self.tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //done: дёргать метод загрузки Offers
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [BUDATAMANAGER processAllActiveOffers];
    });
    
    //    [self.tableView reloadData];
}
- (void)dealloc;
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Common methods


-(void)refreshTable;
{
    [BUDATAMANAGER loadOffersOfType:_status];
    
    
}

/**
 Notification что загрузка завершена (или не нужна)

 @param notification - объект нотификации
 */
- (void)loadedOffers:(NSNotification *)notification;
{
    NSNumber *type = notification.userInfo[@"type"];
    
    switch (type.integerValue) {
        case ApiClientOfferStatusNew: {
            [self.createdRefreshControl endRefreshing];
            
//            if ([notification.userInfo[@"success"] boolValue]) {
                [self.createdTableView reloadData];
//            }
            break;
        }
        case ApiClientOfferStatusActive: {
            [self.activeRefreshControl endRefreshing];
            
//            if ([notification.userInfo[@"success"] boolValue]) {
                [self.activeTableView reloadData];
//            }
            
            
            break;
        }
        case ApiClientOfferStatusCompleted: {
            [self.completedRefreshControl endRefreshing];
            
//            if ([notification.userInfo[@"success"] boolValue]) {
                [self.completedTableView reloadData];
//            }
            break;
        }
        default:
            break;
    }
}

/**
 Пришло Notification что User обновился
 */
- (void)userUpdated;
{
    NSNumber *coins = BUDATAMANAGER.currentUser.balance;
    
    NSString *coinsStr = coins.integerValue ? [NSString stringWithFormat:@"%@", coins] : @"0";
    self.coinsLabel.text = coinsStr;
    
//    #warning launch offers reload
    
    [self.createdTableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationNone];
    
    //done: дёргать метод загрузки Offers
    [BUDATAMANAGER loadOffersOfType:_status];
    
    //TODO: настройка Партнёрок
    if (!_partnersInitialized && [BUDATAMANAGER isTokenStored]) {
        //APPLOVIN
        [ALSdk initializeSdk];
        [[ALSdk shared] setUserIdentifier:BUDATAMANAGER.currentUser.access_token];
        
        
        //FYBER
        FYBSDKOptions *fyberOptions = [FYBSDKOptions optionsWithAppId:@"106367"
                                                               userId:BUDATAMANAGER.currentUser.access_token
                                                        securityToken:@"5ff0b285586397645f8d4f897636c202"];
        
        [FyberSDK startWithOptions:fyberOptions];
        
        
        //NativeX
        [NativeXSDK initializeWithAppId:@"140098"
                     andPublisherUserId:BUDATAMANAGER.currentUser.access_token
                         andSDKDelegate:self];
        [NativeXSDK enableDebugLog:YES];
        
        
        //OfferToro
        [[OfferToro sharedInstance] initOWWithAppId:@"3475"
                                             userId:BUDATAMANAGER.currentUser.access_token
                                          secretKey:@"168b02a441090ae4088ac8cd365aabdc"];
        
        [OfferToro sharedInstance].delegate = self;
        
        _partnersInitialized = YES;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Database
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)databaseModified:(NSNotification *)ignored
{
    // This method is invoked (on the main-thread) after a readWriteTransaction (commit) has completed.
    // The commit could come from any databaseConnection (from our database).
    
    // Our databaseConnection is "frozen" on a particular commit. (via longLivedReadTransaction)
    // We do this in order to provide a stable data source for the UI.
    //
    // What we're going to do now is move our connection from it's current commit to the latest commit.
    // This is done atomically.
    // And we may jump multiple commits when we make this jump.
    // The notifications array gives us the details on each commit.
    //
    // This process/architecture is detailed in the following wiki articles:
    //
    // https://github.com/yaptv/YapDatabase/wiki/LongLivedReadTransactions
    // https://github.com/yaptv/YapDatabase/wiki/YapDatabaseModifiedNotification
    
    NSArray *notifications = [BUDATAMANAGER.uiConnection beginLongLivedReadTransaction];
    
    // Now that we've updated our "frozen" databaseConnection to the latest commit,
    // we need to update the tableViews (mainTableView & searchResultsTableView).
    //
    // Note: The code below is pretty much boiler-plate update code:
    // https://github.com/yaptv/YapDatabase/wiki/Views#full_animation_example
    //
    // The only difference here is that we have 2 tableViews to update (instead of just 1).
    
    if ([notifications count] == 0) {
        [self.createdRefreshControl endRefreshing];
        [self.activeRefreshControl endRefreshing];
        [self.completedRefreshControl endRefreshing];
        
        return;
    }
    
    NSArray *rowChanges = nil;
    
    // Update mainTableView
    
    
    DDLogVerbose(@"Calculating rowChanges for tableView...");
    
    NSString *extension = @"offer-order"; //@"offer-order-new";
    
    DDLogVerbose(@"Extension = %@", extension);
    
    [[BUDATAMANAGER.uiConnection ext:extension] getSectionChanges:NULL
                                                       rowChanges:&rowChanges
                                                 forNotifications:notifications
                                                     withMappings:BUDATAMANAGER.orderMappings];
    
    DDLogVerbose(@"Processing rowChanges for tableView...");
    
    if ([rowChanges count] > 0)
    {
        DDLogVerbose(@"rowChanges count = %li...", (long)[rowChanges count]);
        [_createdTableView beginUpdates];
        [_activeTableView beginUpdates];
        [_completedTableView beginUpdates];
        
        for (YapDatabaseViewRowChange *rowChange in rowChanges)
        {
            UITableView *fromTable;
            UITableView *toTable;
            
            if ([rowChange.originalGroup isEqualToString:@"offer-new"]) {
                fromTable = _createdTableView;
            } else if ([rowChange.originalGroup isEqualToString:@"offer-active"]) {
                fromTable = _activeTableView;
            }  else if ([rowChange.originalGroup isEqualToString:@"offer-completed"]) {
                fromTable = _completedTableView;
            }
            
            if ([rowChange.finalGroup isEqualToString:@"offer-new"]) {
                toTable = _createdTableView;
            } else if ([rowChange.finalGroup isEqualToString:@"offer-active"]) {
                toTable = _activeTableView;
            }  else if ([rowChange.finalGroup isEqualToString:@"offer-completed"]) {
                toTable = _completedTableView;
            }
            
            //todo - instxPath section = 0
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:rowChange.indexPath.row inSection:0];
            NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:rowChange.newIndexPath.row inSection:0];
            
            
            switch (rowChange.type)
            {
                case YapDatabaseViewChangeDelete :
                {
                    DDLogVerbose(@"rowChanges delete %li..., ", (long)indexPath.row);
                    [fromTable deleteRowsAtIndexPaths:@[ indexPath ]
                                     withRowAnimation:UITableViewRowAnimationAutomatic];
                    
                    //check - игнорим
                    
                    break;
                }
                case YapDatabaseViewChangeInsert :
                {
                    DDLogVerbose(@"rowChanges insert %li...", (long)newIndexPath.row);
                    [toTable insertRowsAtIndexPaths:@[ newIndexPath ]
                                     withRowAnimation:UITableViewRowAnimationAutomatic];
                    
                    if (toTable == _activeTableView) {
                        [BUDATAMANAGER checkOfferAtIndex:newIndexPath.row];
                    }
                    break;
                }
                case YapDatabaseViewChangeMove :
                {
                    DDLogVerbose(@"rowChanges move %li to %li ...", (long)indexPath.row, (long)newIndexPath.row);
                    
                    [fromTable deleteRowsAtIndexPaths:@[ indexPath ]
                                     withRowAnimation:UITableViewRowAnimationAutomatic];
                    
//                    [_tableView moveRowAtIndexPath:indexPath
//                                       toIndexPath:newIndexPath];
                    
                    [toTable insertRowsAtIndexPaths:@[ newIndexPath ]
                                   withRowAnimation:UITableViewRowAnimationAutomatic];
                    
                    if (toTable == _activeTableView) {
                        [BUDATAMANAGER checkOfferAtIndex:newIndexPath.row];
                    }
                    break;
                }
                case YapDatabaseViewChangeUpdate :
                {
                    DDLogVerbose(@"rowChanges update %li...", (long)indexPath.row);
                    [fromTable reloadRowsAtIndexPaths:@[ indexPath ]
                                     withRowAnimation:UITableViewRowAnimationNone];
                    
                    //???
                    if (toTable && fromTable != toTable) {
                        [toTable reloadRowsAtIndexPaths:@[ newIndexPath ]
                                         withRowAnimation:UITableViewRowAnimationNone];
                    }
                    
                    if (fromTable == _activeTableView) {
                        [BUDATAMANAGER checkOfferAtIndex:indexPath.row];
                    }
                    
                    if (toTable == _activeTableView && fromTable != toTable) {
                        [BUDATAMANAGER checkOfferAtIndex:newIndexPath.row];
                    }
                    break;
                }
            }
        }
        
        [_createdTableView endUpdates];
        [_activeTableView endUpdates];
        [_completedTableView endUpdates];
    }
    
    [self.createdRefreshControl endRefreshing];
    [self.activeRefreshControl endRefreshing];
    [self.completedRefreshControl endRefreshing];
}

#pragma mark - Actions


- (IBAction)segmentedClick:(id)sender {
    UISegmentedControl *seg = (UISegmentedControl *)sender;
    
    
    if (!seg || seg.selectedSegmentIndex == 0) {
        _status = ApiClientOfferStatusNew;
        
        self.createdTableView.hidden = NO;
        self.activeTableView.hidden = YES;
        self.completedTableView.hidden = YES;
        
        [self.createdTableView reloadData];
    } else if (seg.selectedSegmentIndex == 1) {
        _status = ApiClientOfferStatusActive;
        
        self.createdTableView.hidden = YES;
        self.activeTableView.hidden = NO;
        self.completedTableView.hidden = YES;
        
        [self.activeTableView reloadData];
    } else if (seg.selectedSegmentIndex == 2) {
        _status = ApiClientOfferStatusCompleted;
        
        self.createdTableView.hidden = YES;
        self.activeTableView.hidden = YES;
        self.completedTableView.hidden = NO;
        
        [self.completedTableView reloadData];
    }
    
    [BUDATAMANAGER loadOffersOfType:_status];
    
//    [self.tableView reloadData];
}

- (IBAction)menuButtonClick:(id)sender {
    [[BUDataManager instance].router menuToggle];
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if (section == 0) {
        NSUInteger count = 0;
        
        NSString *group = nil;
        
        if (tableView == _createdTableView)
        {
            group = @"offer-new";
        }
        else if (tableView == _activeTableView)
        {
            group = @"offer-active";
        }
        else if (tableView == _completedTableView)
        {
            group = @"offer-completed";
        }
        
        count = [BUDATAMANAGER.orderMappings numberOfItemsInGroup:group];
        
        DDLogInfo(@"numberOfRowsInSection - group = %@, count = %li", group, (unsigned long)count);
        
        return count;
    } else if (tableView == self.createdTableView) {
        return 4;
    } else {
        return 0;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    //done: reset when lowest part will be ok (Partners + Video of a day)
    if (tableView == self.createdTableView) {
        return 2;
    } else {
        return 1;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectZero];
    
    if (section == 1 && tableView == self.createdTableView) {
        headerView = [[[NSBundle mainBundle] loadNibNamed:@"BUOfferPartnersTableViewHeader" owner:self options:nil] lastObject];
    }
    
    return headerView;

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
{
    if (section == 1 && tableView == self.createdTableView) {
        return 20.0f;
    } else {
        return 0.0f;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0f;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section
{
    if (section == 1 && tableView == self.createdTableView) {
        return 20.0f;
    } else {
        return 0.0f;
    }
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section
{
    return 0.0f;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section;
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectZero];
    
    if (section == 1 && tableView == self.createdTableView) {
        headerView = [[[NSBundle mainBundle] loadNibNamed:@"BUOfferPartnersTableViewHeader" owner:self options:nil] lastObject];
    }
    
    return headerView;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    BUOfferTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:offerCellIdentifier];
    
    if (!cell) {
        cell = [[BUOfferTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:offerCellIdentifier];
    }
    
    if (indexPath.section == 0) {
        __block BUOffer *offer = nil;
        
        [BUDATAMANAGER.uiConnection readWithBlock:^(YapDatabaseReadTransaction *transaction) {
            NSString *group = @""; // @"offer-order-new";

            if (_status == ApiClientOfferStatusNew) {
                group = @"offer-new";
            }
            else if (_status == ApiClientOfferStatusActive)
            {
                group = @"offer-active";
            }
            else if (_status == ApiClientOfferStatusCompleted)
            {
                group = @"offer-completed";
            }
            
            offer = [[transaction ext:@"offer-order"] objectAtIndex:indexPath.row inGroup:group];;
        }];
        
        cell.model = offer;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [cell configure];
    } else {
        BUOffer *off = [BUOffer new];
        off.statusEnum = ApiClientOfferStatusUndefined;
        
        //TODO: Fyber Cell
        if (indexPath.row == 0) //FYBER
        {
            off.title = @"Задания от Fyber";
            off.task_description = @[ @"В данном разделе все предложения\nпредставлены партнерами сервиса!" ];
            off.logo = @"https://www.fyber.com/images/fyber-logo.png";
        }
        //TODO: NativeX Cell
        if (indexPath.row == 1) //NATIVEX
        {
            off.title = @"Задания от NativeX";
            off.task_description = @[ @"В данном разделе все предложения\nпредставлены партнерами сервиса!" ];
            off.logo = @"https://www.help.nativex.com/wp-content/uploads/2017/04/cropped-nativex-icon.png";
        }
        //TODO: OfferTorro Cell
        if (indexPath.row == 2) //OFFERTORO
        {
            off.title = @"Задания от OfferToro";
            off.task_description = @[ @"В данном разделе все предложения\nпредставлены партнерами сервиса!" ];
            off.logo = @"https://www.offertoro.com/images/v2/logo.png";
        }
        //TODO: AppLovin Cell
        else if (indexPath.row == [tableView numberOfRowsInSection:1] - 1) //LAST row - APPLOVIN
        {
            off.title = @"Видео Дня";
            off.task_description = @[@"Если устал, не проблема отвлекись.\nКаждый день новое топовое видео!"];
        }
        
        cell.model = off;
        
        [cell configure];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (indexPath.section == 0) {
        __block BUOffer *offer = nil;
        
        [BUDATAMANAGER.uiConnection readWithBlock:^(YapDatabaseReadTransaction *transaction) {
            NSString *group = @"offer-new";
            
            if (_status == ApiClientOfferStatusNew) {
                group = @"offer-new";
            }
            else if (_status == ApiClientOfferStatusActive)
            {
                group = @"offer-active";
            }
            else if (_status == ApiClientOfferStatusCompleted)
            {
                group = @"offer-completed";
            }
            
            offer = [[transaction ext:@"offer-order"] objectAtIndex:indexPath.row inGroup:group];
            
            DDLogInfo(@"Selected offer - %@", offer);
        }];
        
        BUSingleOfferVC *singleOffer = [BUSingleOfferVC storyboardInstance];
        //done: Activate SingleOffer
        singleOffer.offer = offer;
        [singleOffer configure];
        
        [self.navigationController pushViewController:singleOffer animated:YES];
    } else if (indexPath.section == 1 && tableView == self.createdTableView) {
        
        if (indexPath.row == [tableView numberOfRowsInSection:1] - 1) //AppLovin - LastRow
        {
            //TODO: Add AppLovin
            
            DDLogInfo(@"AppLovin - WILL SHOW AD");
            
//            [ALIncentivizedInterstitialAd setUserIdentifier:BUDATAMANAGER.currentUser.access_token];
            
            if([ALIncentivizedInterstitialAd isReadyForDisplay]){
                // Show call not using a reward delegate.
    //            [ALIncentivizedInterstitialAd show];
                
                DDLogInfo(@"AppLovin - AD AVAILABLE - SHOWING");
                // Show call if using a reward delegate
                [ALIncentivizedInterstitialAd showAndNotify:self];
            }
            else
            {
                // No rewarded ad is currently available. Perform failover logic...
                DDLogInfo(@"AppLovin - NO AD PRELOADED - loading");
                [ALIncentivizedInterstitialAd preloadAndNotify:self];
            }
        }
        else if (indexPath.row == 0) //FYBER
        {
            //TODO: fyber
            DDLogInfo(@"Fyber OfferWall - will be presented");
            
            if ([BUDATAMANAGER isTokenStored]) {
                [[FyberSDK instance] setUserId:BUDATAMANAGER.currentUser.access_token];
                
                DDLogInfo(@"Fyber OfferWall - userId set");
                // Creating an instance of the FYBOfferWallViewController
                FYBOfferWallViewController *offerWallViewController = [FyberSDK offerWallViewController];
                
                // If YES the offer wall will be dismissed after clicking on an offer
                offerWallViewController.shouldDismissOnRedirect = YES;
                
                // Showing the Offer Wall
                [offerWallViewController presentFromViewController:self animated:YES completion:^{
                    // The Offer Wall is successfully presented
                    
                    DDLogInfo(@"Fyber OfferWall - presented");
                } dismiss:^(NSError *error) {
                    // The Offer Wall is dismissed
                    // If an error occurred, the error parameter describes the error otherwise is nil
                    if (!error) {
                        DDLogInfo(@"Fyber OfferWall - dismissed");
                    } else {
                        DDLogWarn(@"Fyber OfferWall - ERROR !!!\n"
                                  "%@", error);
                    }
                }];
            } else {
                DDLogWarn(@"Fyber OfferWall - NO auth_token !!!");
            }
        }
        else if (indexPath.row == 1) //NativeX
        {
            //TODO: NativeX
            [NativeXSDK fetchAdWithPlacement:kAdPlacementPlayerGeneratedEvent andFetchDelegate:self];
        }
        else if (indexPath.row == 2) //OfferTorro
        {
            //TODO: OfferToro
            [[OfferToro sharedInstance] presentOWInViewController:self animation:YES];
        }
        
    }
    
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - AppLovin Ad RewardDelegate

- (void)rewardValidationRequestForAd:(ALAd *)ad didSucceedWithResponse:(NSDictionary *)response;
{
    DDLogInfo(@"AppLovin - SHOW SUCCESS\n"
              "rewardValidationRequestсейForAd: %@\n"
              "didSucceedWithResponse: %@", ad, response);
    
    [BUDATAMANAGER updateUser];
}

/**
 * This method will be invoked if we were able to contact AppLovin, but the user has already received
 * the maximum number of coins you allowed per day in the web UI.
 *
 *  @param ad       Ad which was viewed.
 *  @param response Dictionary containing response data from the server.
 */
- (void)rewardValidationRequestForAd:(ALAd *)ad didExceedQuotaWithResponse:(NSDictionary *)response;
{
    DDLogWarn(@"AppLovin - SHOW FAIL"
              "rewardValidationRequestForAd: %@\n"
              "didExceedQuotaWithResponse: %@", ad, response);
}

/**
 * This method will be invoked if the AppLovin server rejected the reward request.
 * This would usually happen if the user fails to pass an anti-fraud check.
 *
 *  @param ad       Ad which was viewed.
 *  @param response Dictionary containing response data from the server.
 */
- (void)rewardValidationRequestForAd:(ALAd *)ad wasRejectedWithResponse:(NSDictionary *)response;
{
    DDLogWarn(@"AppLovin - SHOW FAIL\n"
              "rewardValidationRequestForAd: %@\n"
              "wasRejectedWithResponse: %@\n", ad, response);
}

/**
 * This method will be invoked if were unable to contact AppLovin, so no ping will be heading to your server.
 *
 *  @param ad           Ad which was viewed.
 *  @param responseCode A failure code corresponding to a constant defined in <code>ALErrorCodes.h</code>.
 */
- (void)rewardValidationRequestForAd:(ALAd *)ad didFailWithError:(NSInteger)responseCode;
{
    DDLogWarn(@"AppLovin - SHOW FAIL"
              "rewardValidationRequestForAd: %@ \n"
              "didFailWithError: %ld", ad, (long)responseCode);
}

#pragma mark - AppLovin Ad LoadDelegate

/**
 * This method is invoked when an ad is loaded by the AdService.
 *
 * This method is invoked on the main UI thread.
 *
 * @param adService AdService which loaded the ad. Will not be nil.
 * @param ad        Ad that was loaded. Will not be nil.
 */
- (void)adService:(ALAdService *)adService didLoadAd:(ALAd *)ad;
{
    DDLogInfo(@"AppLovin - DOWNLOAD SUCESS\n"
              "adService: %@\n"
              "didLoadAd: %@\n", adService, ad);
    
    [ALIncentivizedInterstitialAd showAndNotify:self];
}

/**
 * This method is invoked when an ad load fails.
 *
 * This method is invoked on the main UI thread.
 *
 * @param adService AdService which failed to load an ad. Will not be nil.
 * @param code      An error code corresponding with a constant defined in <code>ALErrorCodes.h</code>.
 */
- (void)adService:(ALAdService *)adService didFailToLoadAdWithError:(int)code;
{
    DDLogWarn(@"AppLovin - DOWNLOAD FAIL\n"
              "adService: %@\n"
              "didFailToLoadAdWithError: %ld\n", adService, (long)code);
    
    [BUDATAMANAGER showAlertErrorWithMessage:@"Не удалось загрузить видео"];
}

#pragma mark - NativeX - new interface delegate methods - all optional

/**
 *  Optional protocol method that is called when the SDK has finished initializing and is ready to fetch/show ads.
 */
- (void)nativeXSDKInitialized;
{
    DDLogInfo(@"NativeX - INITIALIZATION SUCESS");
    
    //TODO: preLoad AD ?
//    [NativeXSDK fetchAdWithPlacement:kAdPlacementPlayerGeneratedEvent andFetchDelegate:self];
}

/**
 *  Optional protocol method that is called when the SDK has failed to initialize for some reason.
 *  @param error    - error object that describes the exact error encountered by the SDK
 */
- (void)nativeXSDKFailedToInitializeWithError:(NSError*)error;
{
    DDLogWarn(@"NativeX - INITIALIZATION FAILED\n"
              "%@\n"
              "DESCRIPTION:"
              "%@",
              error,
              error.localizedDescription);
}

/**
 *  Called when the ad is successfully fetched from NativeX's servers, and is ready to be displayed
 *  @param placementName    - the placement name string of the Ad that was fetched.
 */
- (void) adFetched:(NSString*)placementName;
{
    DDLogInfo(@"NativeX - AD FETCHED\n"
              "SHOWING\n"
              "placementName - %@\n"
              ,
              placementName);
    // show the ad!
    [NativeXSDK showAdWithPlacement:kAdPlacementPlayerGeneratedEvent];
}

/**
 *  Called when there is no ad available at this time.
 *  @param placementName    - the placement name string that has no ads available.
 */
- (void) noAdAvailable:(NSString*)placementName;
{
    DDLogWarn(@"NativeX - NO AD AVAILABLE\n"
              "placementName - %@", placementName);
}
/**
 *  Called when there was an error fetching the ad from NativeX's servers *
 *  @param placementName    - the placement name string of the Ad that failed to fetch.
 *  @param error            - error object that describes the exact error encountered when fetching the ad
 */
- (void) adFetchFailed:(NSString*)placementName withError:(NSError*)error;
{
    DDLogWarn(@"NativeX - AD FETCH FAILED\n"
              "placementName - %@\n"
              "error - %@",
              placementName,
              error);
}

#pragma mark - OfferToro

/// Invoked when OfferWall is successfully initialized
- (void)offertoroOWInitSuccess;
{
    DDLogInfo(@"OfferToro - OfferWall INIT SUCCESS\n");
}

/// Invoked when OfferWall initialization fails
- (void)offertoroOWInitFail:(NSError *)error;
{
    DDLogWarn(@"OfferToro - OfferWall init FAILED\n"
              "error - %@\n",
              error);
}

/// Invoked when OfferWall is presented
- (void)offertoroOWOpened;
{
    DDLogInfo(@"OfferToro - OfferWall presented\n");
}

// Invoked when OfferWall is dismissed
- (void)offertoroOWClosed;
{
    DDLogInfo(@"OfferToro - OfferWall closed\n");
}

// Automatically invoked every several minutes and returns user’s balance (totalCredits) and the amount of credits earned (amount) by the user since the last update.
- (void)offertoroOWCredited:(NSNumber *)amount totalCredits:(NSNumber *)totalCredits;
{
    DDLogInfo(@"OfferToro - OfferWall credited\n"
              "amount - %@\n"
              "total - %@\n",
              amount,
              totalCredits);
}


@end
