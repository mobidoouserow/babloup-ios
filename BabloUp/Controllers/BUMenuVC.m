//
//  BUMenuVC.m
//  BabloUp
//
//  Created by Pavel Wasilenko on 04/04/17.
//  Copyright © 2017 Bars-i-Lis. All rights reserved.
//

#import "BUMenuVC.h"

#import "BUProfilePartVC.h"

@interface BUMenuVC () <UITableViewDelegate, UITableViewDataSource> {
    NSArray *_title;
    NSArray *_image;
}

@property (weak, nonatomic) IBOutlet UIView *navBarBGVew;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *profilePartPlaceholderView;

@property (strong, nonatomic) BUProfilePartVC *profilePartVC;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *menuBGViewWidthConstraint;

@end

@implementation BUMenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //iPhone < 6 size = 282
    
    if ([BUUtility isSmallerThenIPhone6]) {
        self.menuBGViewWidthConstraint.constant = 282.0f;
    }
    
    //array of puncts titles
    _title =
    @[
      @"Задания",
      @"Аккаунт",
      @"Монеты",
      @"Партнёрская программа",
      ];
    
    //array of puncts images
    _image =
    @[
      [UIImage imageNamed:@"Menu-Tasks"],
      [UIImage imageNamed:@"Menu-Account"],
      [UIImage imageNamed:@"Menu-Coins"],
      [UIImage imageNamed:@"Menu-Referrals"],
      ];
    
    //disabling empty cells
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    //disabling scroll
    self.tableView.alwaysBounceVertical = NO;
    self.tableView.scrollEnabled = NO;
    
    //disable muptiple selections:
    self.tableView.allowsMultipleSelection = NO;
    
    //initializing ProfilePartVC
    if (!self.profilePartVC) {
        self.profilePartVC = [BUProfilePartVC storyboardInstance];
    }
    
    
    //DONE: gragient from 66 to 54 in navBarBGVew
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.navBarBGVew.bounds;
    gradient.colors = @[(id)[UIColor menuLogoGradientStartColor].CGColor, (id)[UIColor menuLogoGradientStartColor].CGColor];
    
    [self.navBarBGVew.layer insertSublayer:gradient atIndex:0];
    
    //DONE: logo in navbar
    
    //TableView
    //DONE: select 1st row
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                animated:NO
                          scrollPosition:UITableViewScrollPositionNone];
 
    
    //DONE: disable separator
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    //DONE: selection, interaction with BUBURouterController
    
    //DONE: Disable UI interaction within Profile Part
    [self.profilePartVC.view setUserInteractionEnabled:NO];
    
    //TODO: Load user profile data
    
    //TODO: Load stats from user
}

- (void)viewWillAppear:(BOOL)animated {
    if (![self.childViewControllers containsObject:self.profilePartVC])
    {
        //Adding ProfilePartVC
        [self addChildViewController:self.profilePartVC];
        [self.profilePartVC.view setFrame:self.profilePartPlaceholderView.bounds];
        
        [self.profilePartPlaceholderView addSubview:self.profilePartVC.view];
        [self.profilePartVC didMoveToParentViewController:self];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
//    UITableViewCell *cell = [self tableView:self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
//    
//    [cell setSelected:YES animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return 4;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return  1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"menuCell" forIndexPath:indexPath];
    
    
    cell.backgroundColor = [UIColor menuSeparatorColor];
    
    UILabel *punctLabel = [cell viewWithTag:77];
    
    UIImageView *punctImage = [cell viewWithTag:66];
    
    UIView *bottomSeparatorView = [cell viewWithTag:9];
    
    
    [cell.contentView bringSubviewToFront:bottomSeparatorView];
    
    //puncts
    punctLabel.text = _title[indexPath.row];
    
    punctImage.image = _image[indexPath.row];
    
    
    //selection color
    UIView *bgColorView = [[UIView alloc] initWithFrame:cell.frame];
    
    bgColorView.backgroundColor = [UIColor menuSelectedPunctColor];
    
    CGFloat cellHeight = cell.frame.size.height;
    CGFloat cellWidth = cell.frame.size.width;
    
    CGRect separatorFrame = CGRectMake(0, cellHeight - 1.0f, cellWidth, 1.0f);
    
    UIView *bgColorSeparatorView = [[UIView alloc] initWithFrame:separatorFrame];
    
    bgColorSeparatorView.backgroundColor = [UIColor menuSeparatorColor];
    
    [bgColorView addSubview:bgColorSeparatorView];
    
    
    
    [cell setSelectedBackgroundView:bgColorView];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleDefault];
    
     // Gives black background for you
    
    
    return cell;
}

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    // Remove seperator inset
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//    
//    // Prevent the cell from inheriting the Table View's margin settings
//    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
//        [cell setPreservesSuperviewLayoutMargins:NO];
//    }
//
//    // Explictly set your cell's layout margins
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//}

//- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//    
//    UIView *topSeparatorView = [cell viewWithTag:6];
//    UIView *bottomSeparatorView = [cell viewWithTag:9];
//    
//    topSeparatorView.hidden = YES;
//    [cell.contentView bringSubviewToFront:bottomSeparatorView];
//    return indexPath;
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
        case 0:
            [[BUDataManager instance].router showOffers];
            break;
        case 1:
            [[BUDataManager instance].router showAccount];
            break;
        case 2:
            [[BUDataManager instance].router showCoins];
            break;
        case 3:
            [[BUDataManager instance].router showPartners];
            break;
        default:
            break;
    }
}

@end
