//
//  UIColor+BUAdditions.h
//  Bablo Up
//
//  Generated on Zeplin. (by userow on 4/6/2017).
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (BUAdditions)

//+ (UIColor *)colorWithIntRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue;

+ (UIColor *)bu_greyishBrownColor;
+ (UIColor *)bu_redOrangeColor;
+ (UIColor *)bu_dirtBrownColor;
+ (UIColor *)bu_greyishBrownTwoColor;
+ (UIColor *)bu_paleRedColor;
+ (UIColor *)bu_steelBlueColor;
+ (UIColor *)bu_denimBlueColor;
+ (UIColor *)bu_whiteColor;
+ (UIColor *)bu_greyishBrownFourColor;
+ (UIColor *)bu_blackColor;
+ (UIColor *)bu_darkBlueGreenColor;
+ (UIColor *)bu_flatBlueColor;
+ (UIColor *)bu_sunYellowColor;
+ (UIColor *)bu_softBlueColor;
+ (UIColor *)bu_whiteTwoColor;
+ (UIColor *)bu_oliveDrabColor;
+ (UIColor *)bu_blackTwoColor;
+ (UIColor *)bu_milkChocolateColor;

@end
