//
//  UIColor+BUAdditions.m
//  Bablo Up
//
//  Generated on Zeplin. (by userow on 4/6/2017).
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "UIColor+BUAdditions.h"

@implementation UIColor (BUAdditions)

//+ (UIColor *)colorWithIntRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue
//{
//    UIColor *color = [UIColor  colorWithRed:(CGFloat)red/255.0 green:(CGFloat)green/255.0 blue:(CGFloat)blue/255.0 alpha:1.0];
//    
//    return color;
//}

+ (UIColor *)bu_greyishBrownColor { 
    return [UIColor colorWithWhite:86.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)bu_redOrangeColor { 
    return [UIColor colorWithRed:1.0f green:61.0f / 255.0f blue:0.0f alpha:1.0f];
}

+ (UIColor *)bu_dirtBrownColor { 
    return [UIColor colorWithRed:133.0f / 255.0f green:118.0f / 255.0f blue:53.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)bu_greyishBrownTwoColor { 
    return [UIColor colorWithWhite:66.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)bu_paleRedColor { 
    return [UIColor colorWithRed:216.0f / 255.0f green:75.0f / 255.0f blue:55.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)bu_steelBlueColor { 
    return [UIColor colorWithRed:89.0f / 255.0f green:123.0f / 255.0f blue:160.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)bu_denimBlueColor { 
    return [UIColor colorWithRed:59.0f / 255.0f green:89.0f / 255.0f blue:152.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)bu_whiteColor { 
    return [UIColor colorWithWhite:214.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)bu_greyishBrownFourColor { 
    return [UIColor colorWithWhite:70.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)bu_blackColor { 
    return [UIColor colorWithWhite:20.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)bu_darkBlueGreenColor { 
    return [UIColor colorWithRed:0.0f green:77.0f / 255.0f blue:64.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)bu_flatBlueColor { 
    return [UIColor colorWithRed:55.0f / 255.0f green:122.0f / 255.0f blue:162.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)bu_sunYellowColor { 
    return [UIColor colorWithRed:253.0f / 255.0f green:216.0f / 255.0f blue:53.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)bu_softBlueColor { 
    return [UIColor colorWithRed:109.0f / 255.0f green:185.0f / 255.0f blue:229.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)bu_whiteTwoColor { 
    return [UIColor colorWithWhite:213.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)bu_oliveDrabColor { 
    return [UIColor colorWithRed:118.0f / 255.0f green:105.0f / 255.0f blue:47.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)bu_blackTwoColor { 
    return [UIColor colorWithWhite:54.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)bu_milkChocolateColor { 
    return [UIColor colorWithRed:112.0f / 255.0f green:97.0f / 255.0f blue:32.0f / 255.0f alpha:1.0f];
}

@end
