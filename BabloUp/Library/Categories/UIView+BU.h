//
//  UIView+QS.h
//  QuitSmokingNow
//
//  Created by Pavel Wasilenko on 11/04/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (BU)

@property (nonatomic) IBInspectable CGFloat cornerRadius;
@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic, nullable) IBInspectable UIColor *borderUIColor;

@end
