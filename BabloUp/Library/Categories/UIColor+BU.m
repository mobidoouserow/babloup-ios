//
//  UIColor+BU.m
//  BabloUp
//
//  Created by Pavel Wasilenko on 04/04/17.
//  Copyright © 2017 Bars-i-Lis. All rights reserved.
//

#import "UIColor+BU.h"

@implementation UIColor (BU)

+ (UIColor *)colorWithIntRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue alpha:(NSInteger)alpha;
{
    UIColor *color = [UIColor  colorWithRed:(CGFloat)red/255.0 green:(CGFloat)green/255.0 blue:(CGFloat)blue/255.0 alpha:(CGFloat)alpha/100.0];
    
    return color;
}

+ (UIColor *)colorWithIntRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue;
{
    UIColor *color = [UIColor colorWithIntRed:red green:green blue:blue alpha:100];
    return color;
}

+ (UIColor *)statusBarBackgroundColor
{
    UIColor *color = [UIColor colorWithWhite:20.0/255.0 alpha:1.0];
    
    return color;
}

+ (UIColor *)menuSeparatorColor
{
    UIColor *color = [UIColor colorWithWhite:20.0/255.0 alpha:1.0];
    
    return color;
}

+ (UIColor *)menuLogoGradientStartColor
{
    UIColor *color = [UIColor colorWithWhite:66.0/255.0 alpha:1.0];
    
    return color;
}

+ (UIColor *)menuLogoGradientEndColor
{
    UIColor *color = [UIColor colorWithWhite:54.0/255.0 alpha:1.0];
    
    return color;
}

+ (UIColor *)menuBackgroundColor
{
    UIColor *color = [UIColor colorWithWhite:54.0/255.0 alpha:1.0];
    
    return color;
}

+ (UIColor *)menuSelectedPunctColor
{
    UIColor *color = [UIColor colorWithIntRed:133
                                        green:118
                                         blue:53];
    
    return color;
}

+ (UIColor *)avatarBorderColor
{
    UIColor *color = [UIColor colorWithIntRed:251
                                        green:36
                                         blue:7];
    
    return color;
}

+ (UIColor *)avatarSelectedBorderColor
{
    UIColor *color = [UIColor colorWithIntRed:250
                                        green:210
                                         blue:42];
    
    return color;
}

+ (UIColor *)buttonSelectedBackgroundColor;
{
    UIColor *color = [UIColor colorWithIntRed:55
                                        green:122
                                         blue:162];
    
    return color;
}

+ (UIColor *)buttonBackgrounddColor;
{
    UIColor *color = [UIColor colorWithWhite:90.0f / 255.0f alpha:1.0f];
    
    return color;
}

+ (UIColor *)buttonSelectedBorderColor;
{
    UIColor *color = [UIColor colorWithWhite:1.0 alpha:0.73];
    
    return color;
}
@end
