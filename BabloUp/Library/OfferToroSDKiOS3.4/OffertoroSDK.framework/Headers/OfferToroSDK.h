//
//  OfferToroSDK.h
//  OfferToroSDK
//
//  Created by Yurii Goroshenko on 4/7/16.
//  Copyright © 2016 Yurii Goroshenko. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for OfferToroSDK.
FOUNDATION_EXPORT double OfferToroSDKVersionNumber;

//! Project version string for OfferToroSDK.
FOUNDATION_EXPORT const unsigned char OfferToroSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <OfferToroSDK/PublicHeader.h>

#import "OfferToro.h"

