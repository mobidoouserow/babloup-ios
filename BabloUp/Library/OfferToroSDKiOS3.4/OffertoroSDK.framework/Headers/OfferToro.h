//
//  OfferToro.h
//  OfferToroSDK
//
//  Created by Yurii Goroshenko on 4/7/16.
//  Copyright © 2016 Yurii Goroshenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol OfferToroDelegate <NSObject>

@optional
// Offer Wall
- (void)offertoroOWInitSuccess;

- (void)offertoroOWInitFail:(NSError *)error;

- (void)offertoroOWOpened;

- (void)offertoroOWClosed;

- (void)offertoroOWCredited:(NSNumber *)amount totalCredits:(NSNumber *)totalCredits;

// Rewards Video
- (void)offertoroRWInitSuccess;

- (void)offertoroRWInitFail:(NSError *)error;

- (void)offertoroRWStart;

- (void)offertoroRWAvailability:(BOOL)isAvailable;

- (void)offertoroRWCredited:(NSNumber *)amount;

// Non Incent
- (void)offertoroNIInitSuccess;

- (void)offertoroNIInitFail:(NSError *)error;

- (void)offertoroNIOpened;

- (void)offertoroNIClosed;

// Surveys Wall
- (void)offertoroSWInitSuccess;

- (void)offertoroSWInitFail:(NSError *)error;

- (void)offertoroSWOpened;

- (void)offertoroSWClosed;

- (void)offertoroSWCredited:(NSNumber *)amount totalCredits:(NSNumber *)totalCredits;

@end



@interface OfferToro : NSObject

@property (weak, nonatomic) id<OfferToroDelegate> delegate;

+ (OfferToro *)sharedInstance;

+ (UIStoryboard *)storyboard;

// Offer Wall
- (void)initOWWithAppId:(NSString *)appId userId:(NSString *)userId secretKey:(NSString *)secretKey;

- (void)presentOWInViewController:(id)viewController animation:(BOOL)animation;

- (void)getOWTotalCredits;

// Rewards Video
- (void)initRVWithAppId:(NSString *)appId userId:(NSString *)userId secretKey:(NSString *)secretKey;

- (void)presentRVInViewController:(id)viewController animation:(BOOL)animation;

- (void)getRVAvailability;

// Non Incent
- (void)initNIWithAppId:(NSString *)appId userId:(NSString *)userId secretKey:(NSString *)secretKey;

- (void)presentNIInViewController:(id)viewController animation:(BOOL)animation;

// Surveys Wall
- (void)initSWWithAppId:(NSString *)appId userId:(NSString *)userId secretKey:(NSString *)secretKey;

- (void)presentSWInViewController:(id)viewController animation:(BOOL)animation;

- (void)getSWTotalCredits;
@end
