//
//  BUConstants.h
//  BabloUp
//
//  Created by Pavel Wasilenko on 06/04/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#ifndef BUConstants_h
#define BUConstants_h

#define API_TOKEN_KEYPATH @"API_TOKEN_KEYPATH"

#define API_TOKEN_TEMPORARY @"4cf1f2ca2d2d2dba76fbc90f5eca3c321e99856d94365d803749e94da7884d0b"

//My (bis.nival)
//#define VK_APP_ID @"6025927"

//MobiDoo.io
#define VK_APP_ID @"5995017"

#define VK_APP_SCOPE @[ @"wall", @"email" ]

#define FB_APP_ID @"415948432100472"
#define G_APP_ID @"69926408391-qsug0d1mtdkc1a2vsrn5ab0v5gvpsj3h.apps.googleusercontent.com"

#define STANDARD_ANIMATION_DURATION 0.3f

//DataManager - place to store Current USER
#define CURRENT_USER_KEY @"CURRENT_USER_KEY"

#define DEFAULT_PER_PAGE @500

#define USER_UPDATED_NOTIFICATION @"USER_UPDATED_NOTIFICATION"

#define OFFERS_LOADED_NOTIFICATION @"OFFERS_LOADED_NOTIFICATION"

#endif /* BUConstants_h */
