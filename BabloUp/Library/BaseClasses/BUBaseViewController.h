//
//  BUBaseViewController.h
//  BabloUp
//
//  Created by Pavel Wasilenko on 04/04/17.
//  Copyright © 2017 Bars-i-Lis. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <LGSideMenuController/LGSideMenuController.h>
#import <LGSideMenuController/UIViewController+LGSideMenuController.h>

@interface BUBaseViewController : UIViewController

+ (instancetype)storyboardInstance;

@end
