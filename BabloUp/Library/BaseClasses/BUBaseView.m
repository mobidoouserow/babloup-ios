//
//  BUBaseView.m
//  BabloUp
//
//  Created by Pavel Wasilenko on 05/04/17.
//  Copyright © 2017 Bars-i-Lis. All rights reserved.
//

//@import QuartzCore;

#import "BUBaseView.h"

@implementation BUBaseView

- (void)setCornerRadius:(CGFloat)cornerRadius {
    self.layer.cornerRadius = cornerRadius;
    self.layer.masksToBounds = cornerRadius > 0.001f;
//    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
//    self.layer.shouldRasterize = YES;
}

- (CGFloat)cornerRadius {
    return self.layer.cornerRadius;
}

- (void)setBorderWidth:(CGFloat)borderWidth {
    self.layer.borderWidth = borderWidth;
}

- (CGFloat)borderWidth {
    return self.layer.borderWidth;
}

-(void)setBorderUIColor:(UIColor *)borderUIColor {
    self.layer.borderColor = borderUIColor.CGColor;
}

- (UIColor *)borderUIColor {
    
    UIColor *color;
    
    if (self.layer.borderColor) {
        color = [UIColor colorWithCGColor:(CGColorRef)self.layer.borderColor];
    }
    
    return color;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
