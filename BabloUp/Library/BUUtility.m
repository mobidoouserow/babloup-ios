//
//  BUUtility.m
//  BabloUp
//
//  Created by Pavel Wasilenko on 09/04/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import "BUUtility.h"

@implementation BUUtility

+ (BOOL)isSmallerThenIPhone6 {
    BOOL isSmaller = NO;
    
    if ([UIScreen mainScreen].bounds.size.width < 321.0f) {
        isSmaller = YES;
    }
    
    return isSmaller;
}


@end
