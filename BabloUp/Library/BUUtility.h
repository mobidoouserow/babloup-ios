//
//  BUUtility.h
//  BabloUp
//
//  Created by Pavel Wasilenko on 09/04/17.
//  Copyright © 2017 BarsiLis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BUUtility : NSObject

+ (BOOL)isSmallerThenIPhone6;

@end
